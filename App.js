/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React from 'react';
import {
  AppDrawerNavigation,
  AuthStack,
  AuthLoadingScreen,
} from './src/navigation/AuthNavigator';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {NavigationContainer} from '@react-navigation/native';
import {Root} from 'native-base';

const AppNavigation = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppDrawerNavigation,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
      headerMode: 'none',
    },
  ),
);

export default class App extends React.Component {
  async componentDidMount() {
    console.disableYellowBox = true;
    console.log('ignoredYellowBox : ');
    console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
  }

  render() {
    return (
      <NavigationContainer>
        <Root>
          <AppNavigation />
        </Root>
      </NavigationContainer>
    );
  }
}

