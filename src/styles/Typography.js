export const ToolbarHeader = {
    color: '#212B36',
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
    backgroundColor:'#F8F9FA',
    padding:0
}

export const ToolBarSubheader = {
    textTransform: "capitalize",
    color: '#212B36',
    fontFamily: 'Roboto-Medium',
    fontWeight: "400",
    fontSize: 14,
}

export const FabButtonPrimary = {
    backgroundColor: '#FE3852',
    marginBottom:30
}
