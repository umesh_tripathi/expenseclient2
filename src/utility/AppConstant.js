export default AppConstant = {
  ImageConstant: {
    ic_checked: require("../../assets/ic_checked.png"),
    ic_unchecked: require("../../assets/ic_unchecked.png"),
    ic_silhouette: require("../../assets/ic_silhouette.jpg"),
    ic_circle: require("../../assets/ic_circle.png"),
    ic_whatsapp: require("../../assets/ic_whatsapp.png"),
    ic_calender: require("../../assets/ic_calender.png"),
    ic_downarrow: require("../../assets/ic_downarrow.png"),
    ic_phone:require("../../assets/ic_phone.png"),
    ic_placeholder:require("../../assets/ic_placeholder.png"),
  },
  colorConstant:{
    rcTintColor:"#FE3852"
  }
};
