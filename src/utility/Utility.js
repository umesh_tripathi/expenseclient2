"use strict";
import React from "react";
import { AlertIOS, Toast, Platform, Linking, Alert,ToastAndroid } from "react-native";

class Utility {
  showAlert = async msg => {
    alert(msg);
  };

  showToastShort = async (text) => {
    ToastAndroid.showWithGravity(
      text,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
  };

  showToastLong = async (text) => {
    ToastAndroid.showWithGravity(
      text,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
    );
  };
  

  phoneCall(phone) {
    let phoneNumber = phone;
    if (Platform.OS !== "android") {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert("Cannot make a call");
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  openEmail(email){
    let url = 'mailto:' + email +'?subject=Support';
    Linking.openURL(url).then((data) => {

    }).catch(() => {
      Alert.alert('Make sure mail app installed on your device');
    });
  }

  openWhatsApp(phone){
    let url = 'whatsapp://send?phone=91' + phone;
    Linking.openURL(url).then((data) => {

    }).catch(() => {
      Alert.alert('Make sure whatsapp installed on your device');
    });
  }

  extention(filename){
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
  }

  formatDate(date){
    const arrMonth = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var formatedDate =  date.getDate() +
    "-" +
    arrMonth[(date.getMonth())] +
    "-" +
    date.getFullYear();
    return formatedDate;
  }
  capitalize(str){
    var capitalizeStr = str+""
    if(capitalizeStr.length >= 2){
      capitalizeStr = capitalizeStr.charAt(0).toUpperCase() + capitalizeStr.slice(1)
    }else{
      capitalizeStr = capitalizeStr.charAt(0).toUpperCase() 
    }
    return capitalizeStr;
    }

    titleCase(str) {
      var splitStr = str.toLowerCase().split(' ');
      for (var i = 0; i < splitStr.length; i++) {
          // You do not need to check if i is larger than splitStr length, as your for does that for you
          // Assign it back to the array
          splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
      }
      // Directly return the joined string
      return splitStr.join(' '); 
   }

    removeSpecialCharacter(str){
      var clearedStr = str+"".replace(/[^0-9]/g,"")
      return clearedStr;
    }
}



const utility = new Utility();

const createFormData = (fileKey, fileObject, body,isArray) => {
  const data = new FormData();
 
  if(isArray == true){
    var fileCount = 0
    fileObject.forEach(key => {
      data.append(fileKey,{
        name: "IMGBill"+fileCount+Math.floor(Date.now() / 1000),
        type: 'image/jpeg',
        uri:
          Platform.OS === "android"
            ? key.uri
            : key.uri.replace("file://", "")
      });
      fileCount = fileCount + 1;
    })
   
  }else{
    data.append(fileKey, {
      name: "IMGfileKey"+Math.floor(Date.now() / 1000),
      type: 'image/jpeg',
      uri:
        Platform.OS === "android"
          ? fileObject.uri
          : fileObject.uri.replace("file://", "")
    });
  }

  Object.keys(body).forEach(key => {
    data.append(key,body[key]);
  });

  return data;
};

export { utility, createFormData };
