import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {
  Button,
  Icon,
  View,
  Thumbnail,
  ListItem,
  Left,
  Body,
  Text,
  Right,
} from 'native-base';

import {FormStyle} from '../styles';

import {
  isValid,
  userPreferences,
  utility,
  Enums,
  AppConstant,
  ProgressiveImage,
} from '../utility';

class FileItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      pressHandler,
      fileData,
      index,
      type,
      disabled,
      viewFileHandler,
    } = this.props;
    return (
      <>
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => viewFileHandler(fileData.uri)}
            activeOpacity={1}
            style={styles.touchImage}>
            <ProgressiveImage
              thumbnailSource={{
                uri: fileData.uri,
              }}
              source={{
                uri: fileData.uri,
              }}
              style={styles.listImage}
              resizeMode="cover"
            />
            {/* <Thumbnail
              square
              source={{ uri: fileData.uri }}
              style={styles.listImage}
            /> */}
          </TouchableOpacity>
          <Text style={styles.listText}>{fileData.filename}</Text>
          <Button
            transparent
            onPress={() => pressHandler(index, type)}
            style={FormStyle.listButton}
            disabled={disabled}>
            <Icon name="ios-trash" style={FormStyle.attachIcon} />
          </Button>
        </View>
        <View style={styles.separator}></View>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
  },
  separator: {
    flex: 1,
    height: 1,
    marginLeft: 65,
    backgroundColor: '#637381',
  },
  touchImage: {
    maxHeight: 40,
    maxWidth: 40,
    padding: 5,
    margin: 5,
  },
  listImage: {height: 40, width: 40},
  listButton: {
    marginRight: 5,
  },
  listText: {
    flex: 1,
    marginLeft: 12,
    marginRight: 12,
    color: '#637381',
    fontSize: 15,
    fontFamily: 'Roboto-Medium',
  },
});

export default FileItem;
