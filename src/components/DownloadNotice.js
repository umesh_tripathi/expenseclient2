import React, {PureComponent} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
const {width} = Dimensions.get('window');
import {View, Text, Button} from 'native-base';
import RNFetchBlob from 'rn-fetch-blob';
class DownloadNotice extends PureComponent {
  constructor(props) {
    super(props);
    this.openFilemanager = this.openFilemanager.bind(this);
  }
  openFilemanager() {
    const {localUrlPath} = this.props;
    const android = RNFetchBlob.android;
    if (localUrlPath != null) {
      android.actionViewIntent(localUrlPath, 'vnd.android.document/directory');
    }
  }

  render() {
    const {closeHandler} = this.props;
    return (
      <View style={styles.downloadContainer}>
        <Text style={styles.downloadText}>
          Your file has been downloaded to downloads folder.
        </Text>
        <Button
          small
          dark
          transparent
          style={styles.optionbtn}
          onPress={() => {
            this.openFilemanager();
          }}>
          <Text>Show</Text>
        </Button>
        <Button
          small
          dark
          transparent
          style={styles.optionbtn}
          onPress={() => {
            closeHandler();
          }}>
          <Text>Cancel</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  downloadContainer: {
    backgroundColor: '#FFFFFF',
    height: 100,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
   
    width,
    position: 'absolute',
    bottom: 0,
  },
  downloadText: {
    color: '#000000',
  },
  optionbtn: {
    borderWidth: 2,
    borderRadius: 5,
    borderColor: '#DFDFDF',
  },
});
export default DownloadNotice;
