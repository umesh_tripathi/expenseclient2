import React, {Component} from 'react';
import {StyleSheet, AsyncStorage, TouchableOpacity} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import {DrawerActions} from 'react-navigation-drawer';
import {NavigationEvents} from 'react-navigation';
import {
  Container,
  Content,
  Header,
  Title,
  Button,
  Text,
  Left,
  Body,
  Icon,
  Grid,
  Row,
  Col,
  Thumbnail,
  Subtitle,
  StyleProvider,
  List,
  ListItem,
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import commonColors from '../../native-base-theme/variables/commonColor';
import {ToolbarHeader, ToolBarSubheader} from '../styles';
import {userPreferences,utility} from '../utility';

// const EASideBar = props => {
class EASideBar extends Component {
  constructor(props) {
    super(props);
    //this.
    this.willFocusSideBar = null;
    this.redirectToProfile = this.redirectToProfile.bind(this);
  }

  state = {
    appRoutes: [
      {
        key: 'Home',
        params: undefined,
        routeName: 'Home',
      },
      {
        key: 'Shops',
        params: undefined,
        routeName: 'Shops',
      },
      {
        key: 'Profile',
        params: undefined,
        routeName: 'Profile',
      },
      {
        key: 'Support',
        params: undefined,
        routeName: 'Support',
      },
    ],
    userName: '',
    businessName: '',
    profileImage: null,
    shopName: '',
    isMounted: false,
  };

  setUserData = async () => {
    let firstName = await userPreferences.getPreferences(
      userPreferences.firstName,
    );
    let lastName = await userPreferences.getPreferences(
      userPreferences.lastName,
    );
    let shopName = await userPreferences.getPreferences(
      userPreferences.userShopName,
    );
    let businessName = await userPreferences.getPreferences(
      userPreferences.businessName,
    );

    let profileImage = await userPreferences.getPreferences(
      userPreferences.profilePhoto,
    );

    if (firstName != null && lastName != null) {
      this.setState({
        userName: utility.titleCase(firstName) + ' ' + utility.titleCase(lastName),
        businessName: businessName,
        shopName: shopName,
        profileImage: profileImage,
      });
    }
  };

  async componentDidMount() {
    this.setState({isMounted: true});
    this.setUserData();
    this.willFocusSideBar = this.props.navigation.addListener(
      'didFocus',
      this.handleTabFocus,
    );
  }

  async componentWillUnmount() {
    this.setState({isMounted: false});
    if (this.willFocusSideBar != null) {
      this.willFocusSideBar.remove();
    }
  }

  async UNSAFE_componentWillReceiveProps(nextProps) {
    try {
      if (this.state.isMounted) {
        this.setUserData();
      }
    } catch (err) {
      console.log('err : ', err);
    }
  }

  handleTabFocus = () => {
    this.setUserData();
  };

  redirectToPath = path => {
    this.props.navigation.closeDrawer();
    this.props.navigation.navigate(path);
  };

  redirectToProfile = async () => {
    this.props.navigation.closeDrawer();
    this.props.navigation.navigate('Profile');
  };

  renderProfileImage() {
    if (this.state.profileImage == null || this.state.profileImage == '' || this.state.profileImage == 'null') {
      return (
        <TouchableOpacity
          onPress={() => this.redirectToProfile()}
          activeOpacity={1}>
          <Thumbnail
            circle
            large
            source={AppConstant.ImageConstant.ic_silhouette}
            style={styles.profileImage}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => this.redirectToProfile()}
          activeOpacity={1}>
          <Thumbnail
            circle
            large
            source={{uri: this.state.profileImage}}
            style={styles.profileImage}
          />
        </TouchableOpacity>
      );
    }
  }

  signOutAsync = async () => {
    userPreferences.clearPreferences();
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };

  render() {
    return (
      <StyleProvider style={getTheme(commonColors)}>
        <SafeAreaView style={{flex: 1}}>
          <Header span style={styles.topHeader}>
            <Grid>
              <Row>
                <Col size={40}>{this.renderProfileImage()}</Col>
                <Col size={60}>
                  <Row style={styles.titleFlex}>
                    <TouchableOpacity
                      onPress={() => this.redirectToProfile()}
                      activeOpacity={1}>
                      <Title style={styles.headerColor}>
                        {this.state.userName}
                      </Title>
                    </TouchableOpacity>
                    <Subtitle style={styles.subHeaderColor}>
                      {this.state.businessName}
                    </Subtitle>
                    <Subtitle style={styles.subHeaderColor}>
                      {this.state.shopName
                        ? '(' + this.state.shopName + ')'
                        : ''}
                    </Subtitle>
                  </Row>
                </Col>
              </Row>
            </Grid>
          </Header>
          <Content padder contentContainerStyle={styles.container}>
            <List style={{width: '100%'}}>
              {this.state.appRoutes.map(route => {
                return (
                  <ListItem
                    button
                    onPress={() => this.redirectToPath(route.routeName)}
                    key={route.key}
                    noBorder>
                    <Text>{route.routeName}</Text>
                  </ListItem>
                );
              })}
              <ListItem button onPress={this.signOutAsync} noBorder>
                <Text>Log Out</Text>
              </ListItem>
            </List>
          </Content>
        </SafeAreaView>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  headerColor: ToolbarHeader,
  subHeaderColor: ToolBarSubheader,
  titleFlex: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
    alignItems: 'flex-start',
  },
  topHeader: {
    paddingTop: 20,
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    fontFamily: 'Roboto',
  },
});

export default EASideBar;
