import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import {
  Button,
  Icon,
  Text,
  Grid,
  Row,
  Col,
  View,
  Thumbnail
} from "native-base";

import {
  userPreferences,
  utility,
  Enums,
  AppConstant
} from "../utility";

const EAListItem = ({
  supplier,
  billInfo,
  paymentInfo,
  thirdLine,
  secondLineIcon,
  debitText,
  creditText,
  showNegative,
  type,
  pressHandler,
  linkHandler
}) => {
  if (type == 1) {
    //Supplier listing
    return (
      <TouchableOpacity
        onPress={() => pressHandler(supplier)}
        activeOpacity={1}
      >
        <Grid style={styles.item}>
          <Row style={styles.row}>
            <Col size={70}>
              <Text style={styles.title}>
                {supplier.supplier_name.length <= 18
                  ? supplier.supplier_name
                  : supplier.supplier_name.slice(0, 18) + "..."}
              </Text>
            </Col>
            <Col size={30} style={styles.amountCol}>
              <Text
                style={
                  supplier.outstanding_amount == 0
                    ? [styles.amountText]
                    : [styles.amountText, styles.dangerText]
                }
              >
                {'₹'+supplier.outstanding_amount}
              </Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Row>
                <Button
                  small
                  transparent
                  style={styles.listButton}
                  onPress={() => linkHandler(supplier, 1)}
                >
                  <Thumbnail
                    source={AppConstant.ImageConstant.ic_phone}
                    style={styles.iconText}
                  />
                </Button>
                <Button
                  small
                  transparent
                  style={styles.imageButton}
                  onPress={() => linkHandler(supplier, 2)}
                >
                  <Thumbnail
                    source={AppConstant.ImageConstant.ic_whatsapp}
                    style={styles.iconImage}
                  />
                </Button>
              </Row>
            </Col>
            <Col style={styles.amountCol}>
              <Row>
                <Text style={[styles.amountText, styles.subHeader]}>
                  {"Outstanding"}
                </Text>
              </Row>
            </Col>
          </Row>
        </Grid>
      </TouchableOpacity>
    );
  } else if (type == 2) {
    var billStatus = "Unsettled";
    if (billInfo.bill_status == 1) {
      billStatus = "Unsettled";
    } else if (billInfo.bill_status == 2) {
      billStatus = "Settled";
    } else if (billInfo.bill_status == 3) {
      billStatus = "Partially Settled";
    }

    //Bill listing
    return (
      <TouchableOpacity
        onPress={() => pressHandler(billInfo)}
        activeOpacity={1}
      >
        <Grid style={styles.item}>
          <Row style={styles.row}>
            <Col size={70}>
              <Text style={styles.billTitle}>
                {billInfo.bill_number.length <= 18
                  ? billInfo.bill_number
                  : billInfo.bill_number.slice(0, 18) + "..."}
              </Text>
            </Col>
            <Col size={30} style={styles.amountCol}>
              {billInfo.bill_status == 2 ? (
                <Text style={styles.amountText}>{'₹'+billInfo.bill_amount}</Text>
              ) : (
                <Text style={[styles.amountText, styles.dangerText]}>
                  {'₹'+billInfo.bill_amount}
                </Text>
              )}
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Row>
                <Text style={styles.subHeader}>
                {billInfo.supplier_name.length <= 18
                  ? billInfo.supplier_name
                  : billInfo.supplier_name.slice(0, 18) + "..."}
                </Text>
              </Row>
            </Col>
            <Col style={styles.amountCol}>
              <Text style={[styles.amountText, styles.subHeader]}>
                {billStatus}
              </Text>
            </Col>
          </Row>
        </Grid>
      </TouchableOpacity>
    );
  } else {
    var paymentName = "";
    if (
      paymentInfo.transaction_type == 3 &&
      paymentInfo.supplier_name != null
    ) {
      paymentName = paymentInfo.supplier_name;
    } else {
      paymentName =
        paymentInfo.category_name != null ? paymentInfo.category_name : "";
    }
    //Payment listing
    return (
      <TouchableOpacity
      onPress={() => pressHandler(paymentInfo)}
      activeOpacity={1}
    >
      <Grid style={styles.item}>
        <Row style={styles.row}>
          <Col size={70}>
            <Text style={styles.title}>
              {paymentName.length <= 18
                ? paymentName
                : paymentName.slice(0, 18) + "..."}
            </Text>
          </Col>
          <Col size={30} style={styles.amountCol}>
            <Text style={styles.amountText}>
              {'₹'+paymentInfo.transaction_amount}
            </Text>
          </Col>
        </Row>
        <Row style={styles.row}>
          <Col>
            <Row>
              <Text style={styles.subHeader}>{paymentInfo.display_date}</Text>
            </Row>
          </Col>
          <Col style={styles.amountCol}></Col>
        </Row>
      </Grid>
      </TouchableOpacity>
    );
  }
};

const styles = StyleSheet.create({
  item: {
    paddingTop: 5,
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    borderBottomWidth: 1,
    borderBottomColor: "#DFDFDF"
  },
  row: {
    marginBottom: 6
  },
  amountCol: {
    alignItems: "flex-end"
  },
  title: {
    fontSize: 18,
    fontFamily:"Roboto-Medium",
    textTransform: "capitalize",
    color: "#2e2e2e"
  },
  billTitle: {
    fontSize: 18,
    fontFamily:"Roboto-Medium",
    textTransform: "uppercase",
    color: "#2e2e2e"
  },
  subHeader: {
    color: "#637381",
    fontSize: 15,
    textTransform: "capitalize",
    fontFamily:"Roboto",
  },
  subHeaderIcon: {
    color: "#637381",
    fontSize: 20
  },
  listButton: {
    margin: 0,
    height: 32,
    width: 32,
    alignItems: "center",
    marginRight: 2
  },
  iconText: {
    alignSelf: "center",
    height: 24,
    width: 24,
    borderRadius: 24 / 2,
    backgroundColor: "#FE3852",
    marginLeft: 2
  },
  imageButton: {
    margin: 0,
    height: 32,
    width: 32,
    alignItems: "center",
    backgroundColor: "transparent"
  },
  iconImage: {
    alignSelf: "center",
    height: 40,
    width: 40
  },
  amountText: {
    fontSize: 18,
    fontFamily:"Roboto-Medium",
    color: "#2e2e2e"
  },
  dangerText: {
    color: "#FE3852"
  },
  displayNone: {
    display: "none"
  }
});

export default EAListItem;
