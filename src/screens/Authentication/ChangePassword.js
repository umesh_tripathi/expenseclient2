import React from 'react';
import {
  View,
  AsyncStorage,
  StyleSheet,
  StatusBar,
  Image,
  ScrollView,
  Platform,
  KeyboardAvoidingView,
  BackHandler,
} from 'react-native';
import {
  Container,
  Content,
  Text,
  Header,
  StyleProvider,
  Button,
  Toast,
} from 'native-base';
import getTheme from '../../../native-base-theme/components';
import commonColors from '../../../native-base-theme/variables/commonColor';
import SafeAreaView from 'react-native-safe-area-view';
import {WelcomeHeader, WelcomeHeaderDark} from './AuthStyles';
import {EATextInput, EASpinner} from '../../components';
import UserService from '../../services/user';
import {
  isValid,
  userPreferences,
  utility,
  Enums,
  createFormData,
} from '../../utility';
import Loader from '../Shared/Loader';

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      password: '',
      isLoading: false,
    };
  }

  static navigationOptions = {
    headerShown: false,
  };

  componentDidMount() {
    const {navigation} = this.props;
    const userInfo = navigation.getParam('userInfo');
    this.setState({userInfo: userInfo});
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    return true;
  };

  onBlurText = (validatorKey, errorKey, stateKey) => () => {
    this.setState({
      [errorKey]: isValid(validatorKey, this.state[stateKey]),
    });
  };

  // onBlurText = (validatorKey, errorKey, stateKey) => () => {
  //   this.setState({
  //     [errorKey]: isValid(validatorKey, this.state[stateKey])
  //   });
  // };

  validate = async () => {
    let status = {valid: true, message: ''};
    let passwordError = isValid('password', this.state.password);
    let promise = new Promise((resolve, reject) => {
      this.setState(
        {
          passwordError,
        },
        () => {
          if (this.state.passwordError) {
            status.valid = false;
            status.message = passwordError;
          }
          resolve(status);
        },
      );
    });

    return promise;
  };

  changePassword = async () => {
    try {
      let status = await this.validate();
      if (!status.valid) {
        Toast.show({
          text: `${status.message}!`,
          buttonText: 'Ok',
          position: 'bottom',
          type: 'danger',
          duration: 5000,
        });
      } else {
        this.setState({isLoading: true});
        let serverCallUser = await UserService.changePassword({
          userId: this.state.userInfo.id,
          change_password: this.state.password,
        });

        if (serverCallUser.status == 0) {
          var msg = serverCallUser.msg;
          this.setState({isLoading: false});
          Toast.show({
            text: msg,
            buttonText: 'Ok',
            type: 'danger',
            duration: 5000,
          });
        } else {
          let auth = await UserService.login({
            email_id: this.state.userInfo.email_id,
            password: this.state.password,
          });

          if (auth.status == 0) {
            var msg = auth.msg;
            this.setState({isLoading: false});
            Toast.show({
              text: msg,
              buttonText: 'Ok',
              type: 'danger',
              duration: 5000,
            });
          } else {
            var msg = auth.msg;
            userPreferences.setPreferences(
              userPreferences.authToken,
              'Bearer ' + auth.token,
            );
            userPreferences.setPreferences(
              userPreferences.userId,
              auth.users.id + '',
            );
            userPreferences.setPreferences(
              userPreferences.firstName,
              auth.users.first_name,
            );
            userPreferences.setPreferences(
              userPreferences.lastName,
              auth.users.last_name,
            );
            userPreferences.setPreferences(
              userPreferences.emailId,
              auth.users.email_id,
            );
            userPreferences.setPreferences(
              userPreferences.phoneNumber,
              auth.users.phone_number + '',
            );
            userPreferences.setPreferences(
              userPreferences.address,
              auth.users.address,
            );
            userPreferences.setPreferences(
              userPreferences.businessName,
              auth.users.business_name,
            );
            userPreferences.setPreferences(
              userPreferences.profilePhoto,
              auth.users.profile_photo,
            );
            this.setState({isLoading: false});
            if (auth.shop_flag == 1) {
              if (auth.shop != null && auth.shop.length == 1) {
                let shopInfo = auth.shop[0];
                await userPreferences.setPreferences(
                  userPreferences.userShopId,
                  shopInfo.id + '',
                );
                await userPreferences.setPreferences(
                  userPreferences.userShopName,
                  shopInfo.shop_name + '',
                );
                this.props.navigation.navigate('App');
              } else {
                if (auth.shop != null && auth.shop.length >= 1) {
                  var shopInfo = auth.shop[0];

                  await userPreferences.setPreferences(
                    userPreferences.userShopId,
                    shopInfo.id + '',
                  );

                  await userPreferences.setPreferences(
                    userPreferences.userShopName,
                    shopInfo.shop_name + '',
                  );
                }
                this.props.navigation.navigate('Shops');
              }
            } else {
              this.props.navigation.navigate('AddShop', {firstTime: 1});
            }
          }
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text:
            error && error.message
              ? error.message
              : error || 'Not Valid Error!',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  redirectBack = () => {
    this.props.navigation.navigate('ForgotPassword');
  };

  onChangeText = key => text => {
    this.setState({
      [key]: text,
    });
  };

  renderChangePassword = () => {
    return (
      <Content padder contentContainerStyle={styles.container} enabled>
        {Platform.OS === 'ios' ? (
          <></>
        ) : (
          <Header noShadow style={styles.header}></Header>
        )}
        <View style={styles.logoContainer}>
          {/* <Image
            style={styles.logoView}
            source={require('../../../assets/icon.png')}
          /> */}
          <Text style={WelcomeHeader}>
            fingerchips <Text style={WelcomeHeaderDark}></Text>
          </Text>
        </View>

        <View style={styles.inputContainer}>
          <Text style={styles.messageLabel}>Enter new password</Text>
          <EATextInput
            secureTextEntry={true}
            autoCapitalize="none"
            value={this.state.password}
            keyboardType="default"
            placeholder="Password"
            onChangeText={this.onChangeText('password')}
            error={this.state.passwordError}
            onBlur={this.onBlurText('password', 'passwordError', 'password')}
          />
          <Button block onPress={this.changePassword}>
            <Text>Submit</Text>
          </Button>
        </View>
        <View style={styles.forgotContainer}>
          <Button
            transparent
            onPress={this.redirectBack}
            style={{marginTop: 10}}>
            <Text>Back</Text>
          </Button>
        </View>
      </Content>
    );
  };

  render() {
    return (
      <StyleProvider style={getTheme(commonColors)}>
        <SafeAreaView style={styles.container}>
          <StatusBar
            hidden={false}
            barStyle="light-content"
            backgroundColor="#FE3852"
            translucent={true}
          />

          <Container>
            {this.state.isLoading ? <Loader /> : this.renderChangePassword()}
          </Container>
        </SafeAreaView>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
  },
  keyboardviewcontainer: {
    flex: 1,
  },
  header: {
    backgroundColor: '#FFF',
  },
  messageLabel: {
    fontFamily: 'Roboto-Medium',
    fontSize: 15,
    paddingBottom: 8,
    alignSelf: 'center',
    marginBottom: 10,
  },
  logoContainer: {
    flex: 2,
    backgroundColor: '#fff',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  logoView: {
    width: 100,
    height: 100,
  },
  inputContainer: {
    flex: 4,
    backgroundColor: '#fff',
    justifyContent: 'center',
    margin: 30,
  },
  forgotContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});

export default ChangePassword;
