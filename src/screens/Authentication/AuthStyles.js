export const WelcomeHeader = {
    fontFamily: 'Roboto-Black',
    fontSize: 40,
    color: '#FE3852',
    marginTop: 10,
}

export const WelcomeHeaderDark = {
    fontFamily: 'Roboto-Medium',
    fontSize: 40,
    fontWeight: 'bold',
    color: '#212B36',
    marginTop: 10,
}