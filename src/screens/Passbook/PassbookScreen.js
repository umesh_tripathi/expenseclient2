import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Modal,
  FlatList,
  RefreshControl,
  Dimensions
} from 'react-native';
import {
  Container,
  Content,
  Icon,
  Grid,
  Row,
  Picker,
  View,
  H1,
  Toast,
  StyleProvider,
  Fab,
} from 'native-base';
import getTheme from '../../../native-base-theme/components';
import commonColors from '../../../native-base-theme/variables/commonColor';
import {EAButtonGroup, EABricks, EAListItem} from '../../components';
import {FabButtonPrimary} from '../../styles';
const SCREEN_MIN_WIDTH = Math.round((Dimensions.get('window').width - 40) / 2);
import {
  isValid,
  userPreferences,
  utility,
  Enums,
  AppConstant,
} from '../../utility';
import Loader from '../Shared/Loader';
import FilterScreen from '../Shared/FilterScreen';
import PaymentService from '../../services/payments';
import SupplierService from '../../services/supplier';

class PassbookScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null,
      filterModalVisible: false,
      arrSuppliers: [],
      supplierId: null,
      arrCategoriesGeneral: [],
      arrCategoriesCash: [],
      categoryId: null,
      suppliers: [],
      arrPayments: [],
      fromDate: '',
      toDate: '',
      filterType: 0,
      expense: 0,
      cash: 0,
      supplierPayment: 0,
      transactionType: null,
      tempOption: null,
      isLoading: false,
      refreshing: false,
    };
    this.btnFilterTap = this.btnFilterTap.bind(this);
    this.editPassbook = this.editPassbook.bind(this);
    this.filterCompletionHandler = this.filterCompletionHandler.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
  }

  static navigationOptions = {
    headerShown: false,
  };

  handleTabFocus = async () => {
    let passbookTab = await userPreferences.getPreferences(
      userPreferences.passbookTab,
    );
    if (passbookTab != null && passbookTab == '1') {
      await userPreferences.setPreferences(userPreferences.passbookTab, '0');
      this.getSuppliers();
      this.getPayments(0);
    }
  };

  setFilterModalVisible(visible) {
    this.setState({filterModalVisible: visible});
  }

  onValueChange = key => value => {
    if (key === 'transactionType') {
      this.setState(
        {
          transactionType: value,
          supplierId: null,
          categoryId: null,
        },
        () => {
          this.getPayments(0);
        },
      );
    } else {
      this.setState(
        {
          [key]: value,
        },
        () => {
          this.getPayments(0);
        },
      );
    }
  };

  btnFilterTap(tabId) {
    if (tabId == 2) {
      this.setFilterModalVisible(true);
      this.setState({
        fromDate: '',
        toDate: '',
        filterType: tabId,
        arrPayments: [],
        expense: 0,
        cash: 0,
        supplierPayment: 0,
      });
    } else {
      this.setState({fromDate: '', toDate: '', filterType: tabId}, () => {
        this.getPayments(0);
      });
    }
  }

  filterCompletionHandler(type, fromDate, toDate) {
    if (type == 1) {
      this.setState(
        {
          fromDate: fromDate,
          toDate: toDate,
          filterType: 2,
          arrPayments: [],
          expense: 0,
          cash: 0,
          supplierPayment: 0,
        },
        () => {
          this.getPayments(0);
        },
      );
    }
    this.setFilterModalVisible(false);
  }

  async componentDidMount() {
    this.getCategories();
    this.getSuppliers();
    this.getPayments(0);
    this.props.navigation.addListener('didFocus', this.handleTabFocus);
  }

  async componentDidUpdate() {}

  getCategories = async () => {
    try {
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      this.setState({isLoading: true});
      let categoryData = await PaymentService.getCategories(userId);
      this.setState({isLoading: false});
      if (categoryData.status == 0) {
        var msg = categoryData.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else {
        if (categoryData.category != null) {
          var arrCategorieGeneral = [];
          var arrCategorieCash = [];
          for (let i = 0; i < categoryData.category.length; i++) {
            let dicCategorie = {
              key: categoryData.category[i].id,
              text: categoryData.category[i].category_name,
            };
            if (categoryData.category[i].category_type == 0) {
              arrCategorieGeneral.push(dicCategorie);
            } else if (categoryData.category[i].category_type == 1) {
              arrCategorieCash.push(dicCategorie);
            }
          }

          this.setState({
            arrCategoriesGeneral: arrCategorieGeneral,
            arrCategoriesCash: arrCategorieCash,
          });
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  getSuppliers = async () => {
    try {
      let userShopId = await userPreferences.getPreferences(
        userPreferences.userShopId,
      );
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      this.setState({isLoading: true});
      let supplierData = await SupplierService.getSupplierList(
        userId,
        userShopId,
      );
      this.setState({isLoading: false});
      if (supplierData.status == 0) {
        var msg = supplierData.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else {
        if (supplierData.supplier != null) {
          this.setState({
            arrSuppliers: supplierData.supplier,
          });
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  getPayments = async refreshType => {
    try {
      this.setState({
        arrPayments: [],
        expense: 0,
        cash: 0,
        supplierPayment: 0,
      });
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      let userShopId = await userPreferences.getPreferences(
        userPreferences.userShopId,
      );
      var formData = {
        filterType: this.state.filterType,
        fromDate: this.state.fromDate,
        toDate: this.state.toDate,
        transaction_type: this.state.transactionType,
        userId: userId,
        shopId: userShopId,
        supplier_id:this.state.supplierId,
        category_id:this.state.categoryId
      };

      this.setState({
        isLoading: refreshType == 0 ? true : false,
        refreshing: refreshType == 1 ? true : false,
      });
      let serverCallPayments = await PaymentService.getPaymentList(formData);

      this.setState({isLoading: false, refreshing: false});
      if (serverCallPayments.status == 0) {
        var msg = serverCallPayments.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else {
        if (
          serverCallPayments.transaction != null ||
          serverCallPayments.transaction != ''
        ) {
          this.setState({
            arrPayments: serverCallPayments.transaction,
            expense: serverCallPayments.Genral_expense_count,
            cash: serverCallPayments.petty_cash_count,
            supplierPayment: serverCallPayments.supplier_payment_count,
          });
        } else {
          this.setState({
            arrPayments: [],
            expense: 0,
            cash: 0,
            supplierPayment: 0,
          });
        }
      }
    } catch (error) {
      this.setState({isLoading: false, refreshing: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  editPassbook = paymentInfo => {
    this.props.navigation.navigate('AddPayment', {
      formType: 1,
      paymentInfo: paymentInfo,
    });
  };

  renderFilterPayment = () => {
    return (
      <View>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.filterModalVisible}
          style={styles.modal}
          onRequestClose={() => {}}>
          <View style={styles.modal}>
            <FilterScreen
              completionHandler={this.filterCompletionHandler}></FilterScreen>
          </View>
        </Modal>
      </View>
    );
  };

  renderPickerType = () => {
    if (this.state.transactionType == 1 || this.state.transactionType == 2) {
      return (
        <Row style={styles.dropDown}>
          <Icon
            name="keyboard-arrow-down"
            type="MaterialIcons"
            style={styles.pickerIcon}
          />
          <Picker
            note={true}
            mode="dropdown"
            style={{
              backgroundColor: 'transparent',
              width: SCREEN_MIN_WIDTH,
              color: '#FE3852',
            }}
            selectedValue={this.state.categoryId}
            onValueChange={this.onValueChange('categoryId')}>
            <Picker.Item label="Select Category" value={null} key={'null'} />
            {this.state.transactionType == 1
              ? this.state.arrCategoriesGeneral.map((value, index) => {
                  return (
                    <Picker.Item
                      label={value.text}
                      value={value.key}
                      key={value.key + ''}
                    />
                  );
                })
              : this.state.arrCategoriesCash.map((value, index) => {
                  return (
                    <Picker.Item
                      label={value.text}
                      value={value.key}
                      key={value.key + ''}
                    />
                  );
                })}
          </Picker>
        </Row>
      );
    } else if (this.state.transactionType == 3) {
      return (
        <Row style={styles.dropDown}>
          <Icon
            name="keyboard-arrow-down"
            type="MaterialIcons"
            style={styles.pickerIcon}
          />
          <Picker
            note={true}
            mode="dropdown"
            style={{
              backgroundColor: 'transparent',
              width: SCREEN_MIN_WIDTH,
              color: '#FE3852',
            }}
            selectedValue={this.state.supplierId}
            onValueChange={this.onValueChange('supplierId')}>
            <Picker.Item label="Select Supplier" value={null} key={'null'} />
            {this.state.arrSuppliers.map((value, index) => {
              return (
                <Picker.Item
                  label={utility.titleCase(value.supplier_name)}
                  value={value.id}
                  key={value.id + ''}
                />
              );
            })}
          </Picker>
        </Row>
      );
    } else {
      return (
        <Row style={styles.dropDown}>
          <Icon
            name="keyboard-arrow-down"
            type="MaterialIcons"
            style={styles.pickerIcon}
          />
          <Picker
            note={true}
            mode="dropdown"
            style={{
              backgroundColor: 'transparent',
              width: SCREEN_MIN_WIDTH,
              color: '#FE3852',
            }}
            selectedValue={this.state.tempOption}
            onValueChange={this.onValueChange('tempOption')}>
            <Picker.Item label="Select Option" value={null} key={'null'} />
          </Picker>
        </Row>
      );
    }
  };

  renderTransactionTypePicker = () => {
    return (
      <Row style={styles.dropdownContainer}>
        <Row style={styles.dropDown}>
          <Icon
            name="keyboard-arrow-down"
            type="MaterialIcons"
            style={styles.pickerIcon}
          />
          <Picker
            note={true}
            mode="dropdown"
            style={{
              backgroundColor: 'transparent',
              width: SCREEN_MIN_WIDTH,
              color: '#FE3852',
            }}
            selectedValue={this.state.transactionType}
            onValueChange={this.onValueChange('transactionType')}>
            <Picker.Item label="Select Type" value={null} key={'null'} />
            {Enums.paymentType.map((value, index) => {
              return (
                <Picker.Item
                  label={value.text}
                  value={value.key}
                  key={value.key + ''}
                />
              );
            })}
          </Picker>
        </Row>
        {this.renderPickerType()}
      </Row>
    );
  };

  addPassbook = async () => {
    //Form type => 0:Add , 1:Edit
    this.props.navigation.navigate('AddPayment', {formType: 0});
  };

  renderPayment = () => {
    if (this.state.arrPayments.length == 0) {
      return (
        <View style={styles.message}>
          <H1>No Payments Available.</H1>
        </View>
      );
    } else {
      return (
        <>
          <Row style={styles.bricksContainer}>
            <EABricks
              brick1={{
                text1: '₹' + this.state.expense,
                text2: 'General',
                color: '#FE3852',
              }}
              brick2={{
                text1: '₹' + this.state.cash,
                text2: 'Petty',
                color: '#6DD400',
              }}
              brick3={{
                text1: '₹' + this.state.supplierPayment,
                text2: 'Supplier',
                color: '#F7B500',
              }}
            />
          </Row>
          <Row>
            {/* <ScrollView
              contentContainerStyle={{
                flexGrow: 1,
                justifyContent: "space-between"
              }}
            > */}
            <Row>
              <FlatList
                data={this.state.arrPayments}
                renderItem={({item}) => (
                  <EAListItem
                    paymentInfo={item}
                    type={3}
                    pressHandler={this.editPassbook}
                  />
                )}
                keyExtractor={item => item.id + ''}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={() => {
                      this.getPayments(1);
                    }}
                    tintColor={AppConstant.colorConstant.rcTintColor}
                  />
                }
              />
            </Row>
            {/* </ScrollView> */}
          </Row>
        </>
      );
    }
  };

  render() {
    return (
      <>
        <StyleProvider style={getTheme(commonColors)}>
          <Container>
            <Content padder contentContainerStyle={styles.container}>
              <Grid>
                <Row style={styles.buttonGroupSection}>
                  <EAButtonGroup pressHandler={this.btnFilterTap} />
                </Row>
                {this.renderTransactionTypePicker()}
                {this.state.isLoading ? <Loader /> : this.renderPayment()}
              </Grid>
            </Content>
            <View>
              <Fab
                direction="up"
                containerStyle={{}}
                style={styles.fabButton}
                position="bottomRight"
                onPress={this.addPassbook}>
                <Icon name="add" />
              </Fab>
            </View>
          </Container>
        </StyleProvider>
        {this.state.filterModalVisible ? this.renderFilterPayment() : <></>}
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    fontFamily: 'Roboto',
  },
  message: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonGroupSection: {
    height: 60,
  },
  dropdownContainer: {
    flexDirection: 'row',
    height: 50,
    marginBottom: 5,
  },
  dropDown: {
    position: 'relative',
  },
  bricksContainer: {
    height: 90,
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  pickerIcon: {
    color: '#FE3852',
    position: 'absolute',
    alignSelf: 'center',
    right: 10,
    fontSize: 25,
  },
  fabButton: FabButtonPrimary,
});

export default PassbookScreen;
