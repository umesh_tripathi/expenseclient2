import React from 'react';
import {
  View,
  AsyncStorage,
  StyleSheet,
  ScrollView,
  BackHandler,
  Platform,
  Modal,
  KeyboardAvoidingView,
} from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Subtitle,
  Right,
  Body,
  Icon,
  Text,
  Grid,
  Row,
  ActionSheet,
  Toast,
  StyleProvider,
} from 'native-base';
import getTheme from '../../../native-base-theme/components';
import commonColors from '../../../native-base-theme/variables/commonColor';
import {ToolbarHeader, FormStyle} from '../../styles';
import {
  EATextInput,
  EATextLabel,
  EADatePicker,
  EAPicker,
  FileItem,
} from '../../components';
import Loader from '../Shared/Loader';
import CameraScreen from '../Shared/CameraScreen';
import {
  isValid,
  userPreferences,
  utility,
  Enums,
  createFormData,
} from '../../utility';
import BillService from '../../services/bills';
import SupplierService from '../../services/supplier';
import PaymentService from '../../services/payments';
import ImagePicker from 'react-native-image-picker';
import ImageViewer from 'react-native-image-zoom-viewer';

const imageOptions = {
  title: 'Select Image',
  quality: 0.5,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class AddPaymentScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formType: 0,
      paymentInfo: null,
      arrSuppliers: [],
      arrCategoriesGeneral: [],
      arrCategoriesCash: [],
      arrPaymentModes: [],
      arrBills: [],
      paymentImage: null,
      paymentOldImage: null,
      paymentPreviewImage: null,
      supplier: 0,
      paymentAmount: '',
      description: '',
      typeId: 0,
      category: 0,
      date: new Date(),
      mode: 0,
      paymentAmountError: '',
      supplierError: '',
      typeIdError: '',
      categoryError: '',
      dateError: '',
      modeError: '',
      descriptionError: '',
      isLoading: false,
      showPaymentImage: false,
      fullScreenImage: null,
      imageIndex: 0,
    };
    this.fileHandler = this.fileHandler.bind(this);
    this.viewFileHandler = this.viewFileHandler.bind(this);
  }

  static navigationOptions = {
    headerShown: false,
  };

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this.getSuppliers();
    this.getCategories();

    const {navigation} = this.props;
    const formType = navigation.getParam('formType');

    if (formType != undefined) {
      if (formType == 1) {
        const paymentInfo = navigation.getParam('paymentInfo');
        var paymentPreviewImage = null;
        if (
          paymentInfo.transaction_image != '' &&
          paymentInfo.transaction_image != 'null' &&
          paymentInfo.transaction_image != null
        ) {
          let filename = paymentInfo.transaction_image.substring(
            paymentInfo.transaction_image.lastIndexOf('/') + 1,
            paymentInfo.transaction_image.length,
          );
          var dicPaymentImage = {
            uri: paymentInfo.transaction_image,
            filename: filename,
          };
          paymentPreviewImage = dicPaymentImage;
        }

        console.log("paymentInfo.transaction_image : ",paymentInfo.transaction_image)

        this.setState({
          paymentInfo: paymentInfo,
          formType: formType,
          category: paymentInfo.category_id,
          paymentAmount: paymentInfo.transaction_amount + '',
          description: paymentInfo.transaction_description,
          date: new Date(paymentInfo.transaction_date),
          mode: paymentInfo.transaction_mode_id,
          supplier: paymentInfo.supplier_id,
          typeId: paymentInfo.transaction_type,
          paymentOldImage: paymentInfo.transaction_image != '' ? paymentInfo.transaction_image:null,
          paymentPreviewImage: paymentPreviewImage,
        });
      } else {
        this.setState({
          formType: formType,
          billStatus: 1,
        });
      }
    }
  }

  async componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    return false;
  };

  showOption = async () => {
    if (this.state.formType == 0 && this.state.paymentImage != null) {
      Toast.show({
        text: 'You can select max 1 image only.',
        buttonText: 'Ok',
        type: 'danger',
        duration: 5000,
      });
      return;
    } 

    if (this.state.formType == 1 && this.state.paymentImage != null) {
      Toast.show({
        text: 'You can select max 1 image only.',
        buttonText: 'Ok',
        type: 'danger',
        duration: 5000,
      });
      return;
    } 

    if (this.state.formType == 1 && this.state.paymentOldImage != null) {
      Toast.show({
        text: 'You can select max 1 image only.',
        buttonText: 'Ok',
        type: 'danger',
        duration: 5000,
      });
      return;
    } 

      ImagePicker.showImagePicker(imageOptions, response => {
        if (response.didCancel) {
        } else if (response.error) {
        } else {
          let filename = response.uri.substring(
            response.uri.lastIndexOf('/') + 1,
            response.uri.length,
          );
          var fileDic = response;
          fileDic.filename = filename;
          this.setState({
            paymentOldImage: null,
            paymentPreviewImage: null,
            paymentImage: fileDic,
          });
        }
      });
    
  };

  viewFileHandler(imageUri) {
    this.setState({
      fullScreenImage: imageUri,
      imageIndex: 0,
      showPaymentImage: true,
    });
  }

  fileHandler(index, type) {
    this.setState({
      paymentOldImage: null,
      paymentPreviewImage: null,
      paymentImage: null,
    });
  }

  onChangeText = key => text => {
    if (key == 'typeId' && text != 0) {
      this.setState({
        [key + 'Error']: '',
        [key]: text,
        supplier: 0,
        category: 0,
        arrBills: [],
      });
    } else if (key == 'supplier' && text != 0) {
      this.setState(
        {
          [key + 'Error']: '',
          [key]: text,
          arrBills: [],
        },
        () => {
          // this.getSupplierBills();
        },
      );
    } else if (key == 'category' && text != 0) {
      this.setState({
        [key + 'Error']: '',
        [key]: text,
      });
    } else if (key == 'mode' && text != 0) {
      this.setState({
        [key + 'Error']: '',
        [key]: text,
      });
    } else {
      this.setState({
        [key]: text,
      });
    }
  };

  onBlurText = (validatorKey, errorKey, stateKey) => () => {
    this.setState({
      [errorKey]: isValid(validatorKey, this.state[stateKey]),
    });
  };

  getSuppliers = async () => {
    try {
      let userShopId = await userPreferences.getPreferences(
        userPreferences.userShopId,
      );
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      this.setState({isLoading: true});
      let supplierData = await SupplierService.getSupplierList(
        userId,
        userShopId,
      );
      this.setState({isLoading: false});
      if (supplierData.status == 0) {
        var msg = supplierData.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else {
        if (supplierData.supplier != null) {
          var arrSupplier = [];
          for (let i = 0; i < supplierData.supplier.length; i++) {
            let dicSupplier = {
              key: supplierData.supplier[i].id,
              text: supplierData.supplier[i].supplier_name,
            };
            arrSupplier.push(dicSupplier);
          }
          this.setState({
            arrSuppliers: arrSupplier,
          });
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  getCategories = async () => {
    try {
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      this.setState({isLoading: true});
      let categoryData = await PaymentService.getCategories(userId);
      this.setState({isLoading: false});
      if (categoryData.status == 0) {
        var msg = categoryData.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else {
        if (categoryData.category != null) {
          var arrCategorieGeneral = [];
          var arrCategorieCash = [];
          for (let i = 0; i < categoryData.category.length; i++) {
            let dicCategorie = {
              key: categoryData.category[i].id,
              text: categoryData.category[i].category_name,
            };
            if (categoryData.category[i].category_type == 0) {
              arrCategorieGeneral.push(dicCategorie);
            } else if (categoryData.category[i].category_type == 1) {
              arrCategorieCash.push(dicCategorie);
            }
          }

          this.setState({
            arrCategoriesGeneral: arrCategorieGeneral,
            arrCategoriesCash: arrCategorieCash,
          });
        }

        if (categoryData.payment_mode != null) {
          var arrPaymentMode = [];
          for (let i = 0; i < categoryData.payment_mode.length; i++) {
            let dicPaymentMode = {
              key: categoryData.payment_mode[i].id,
              text: categoryData.payment_mode[i].payment_name,
            };
            arrPaymentMode.push(dicPaymentMode);
          }
          this.setState({
            arrPaymentModes: arrPaymentMode,
          });
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  getSupplierBills = async () => {
    if (this.state.supplier == 0) {
      return;
    }

    try {
      this.setState({isLoading: true});
      let billsData = await BillService.getSupplierBills(this.state.supplier);
      this.setState({isLoading: false});
      if (billsData.status == 0) {
        var msg = billsData.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else {
        if (billsData.bills != null) {
          var arrBill = [];
          for (let i = 0; i < billsData.bills.length; i++) {
            let dicBill = {
              key: billsData.bills[i].id,
              text: billsData.bills[i].bill_number,
            };
            arrBill.push(dicBill);
          }
          this.setState({
            arrBills: arrBill,
          });
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  validate = async () => {
    let status = {valid: true, message: ''};
    let dateError = isValid('required', this.state.date);
    let paymentAmountError = isValid('numeric', this.state.paymentAmount);
    let typeIdError =
      this.state.typeId == 0 ? 'Please select payment type' : '';
    let modeError = this.state.mode == 0 ? 'Please select mode' : '';
    let categoryError = '';
    let supplierError = '';

    if (this.state.typeId == 1 || this.state.typeId == 2) {
      categoryError = this.state.category == 0 ? 'Please select catgory' : '';
    } else if (this.state.typeId == 3) {
      supplierError = this.state.supplier == 0 ? 'Please select supplier' : '';
    }

    let promise = new Promise((resolve, reject) => {
      this.setState(
        {
          dateError,
          paymentAmountError,
          supplierError,
          categoryError,
          typeIdError,
          modeError,
        },
        () => {
          if (this.state.dateError) {
            status.valid = false;
            status.message = dateError;
          } else if (this.state.paymentAmountError) {
            status.valid = false;
            status.message = paymentAmountError;
          } else if (this.state.supplierError) {
            status.valid = false;
            status.message = supplierError;
          } else if (this.state.categoryError) {
            status.valid = false;
            status.message = categoryError;
          } else if (this.state.typeIdError) {
            status.valid = false;
            status.message = typeIdError;
          } else if (this.state.modeError) {
            status.valid = false;
            status.message = modeError;
          }
          resolve(status);
        },
      );
    });

    return promise;
  };

  submitPaymentForm = async () => {
    try {
      let status = await this.validate();
      if (!status.valid) {
        Toast.show({
          text: `${status.message}!`,
          buttonText: 'Ok',
          position: 'bottom',
          type: 'danger',
          duration: 5000,
        });
      } else {
        this.setState({isLoading: true});
        let userId = await userPreferences.getPreferences(
          userPreferences.userId,
        );
        let userShopId = await userPreferences.getPreferences(
          userPreferences.userShopId,
        );
        var paymentDate = new Date(this.state.date);
        var paymentDateFormatted =
          paymentDate.getFullYear() +
          '-' +
          (paymentDate.getMonth() + 1) +
          '-' +
          paymentDate.getDate();

        var formData = {
          category_id: this.state.category,
          transaction_amount: parseInt(this.state.paymentAmount),
          transaction_description: this.state.description,
          transaction_date: paymentDateFormatted,
          transaction_mode_id: this.state.mode,
          supplier_id: this.state.supplier,
          bill_id: null,
          bill_status: null,
          userId: userId,
          shop_id: userShopId,
          transaction_type: this.state.typeId,
          transaction_pending_amount: 0,
        };

        if (this.state.formType == 1) {
          formData.id = this.state.paymentInfo.id;
          formData.transaction_old_image = this.state.paymentOldImage;
        }

        let multipartData = formData;

        if (this.state.paymentImage != null) {
          multipartData = await createFormData(
            'transaction_image',
            this.state.paymentImage,
            formData,
            false,
          );
        }

        let serverCallPayment =
          this.state.formType == 0
            ? await PaymentService.addPayment(multipartData)
            : await PaymentService.updatePayment(multipartData);
        this.setState({isLoading: false});
        if (serverCallPayment.status == 0) {
          var msg = serverCallPayment.msg;
          Toast.show({
            text: msg,
            buttonText: 'Ok',
            type: 'danger',
            duration: 5000,
          });
        } else {
          await userPreferences.setPreferences(
            userPreferences.passbookTab,
            '1'
          );
       
          await userPreferences.setPreferences(userPreferences.homeTab, '1');
          this.setState({
            category: 0,
            paymentAmount: '',
            description: '',
            date: new Date(),
            mode: 0,
            paymentImage: null,
            supplier: 0,
            typeId: 0,
          });
          var msg = serverCallPayment.msg;
          Toast.show({
            text: msg,
            buttonText: 'Ok',
            type: 'success',
            duration: 5000,
          });
          this.props.navigation.goBack();
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text:
            error && error.message
              ? error.message
              : error || 'Not Valid Error!',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  renderSupplierPayment() {
    return (
      <>
        <Row style={styles.InputSection}>
          <EATextLabel labelText={'Supplier Name'} />
          <EAPicker
            note
            mode="dropdown"
            selectedValue={this.state.supplier}
            option={this.state.arrSuppliers}
            error={this.state.supplierError}
            //  iosIcon={<Icon name="arrow-down" />}
            onValueChange={this.onChangeText('supplier')}
          />
        </Row>

        {/* <Row style={styles.InputSection}>
          <EATextLabel labelText={"Bill Id"} />
          <EAPicker
            note
            mode="dropdown"
            selectedValue={this.state.billId}
            option={this.state.arrBills}
            error={this.state.billIdError}
            //  iosIcon={<Icon name="arrow-down" />}
            onValueChange={this.onChangeText("billId")}
          />
        </Row> */}
      </>
    );
  }

  renderCategory = () => {
    if (this.state.typeId == 1) {
      return (
        <>
          <Row style={styles.InputSection}>
            <EATextLabel labelText={'Category'} />
            <EAPicker
              note
              mode="dropdown"
              selectedValue={this.state.category}
              option={this.state.arrCategoriesGeneral}
              error={this.state.categoryError}
              //  iosIcon={<Icon name="arrow-down" />}
              onValueChange={this.onChangeText('category')}
            />
          </Row>
        </>
      );
    } else if (this.state.typeId == 2) {
      return (
        <>
          <Row style={styles.InputSection}>
            <EATextLabel labelText={'Category'} />
            <EAPicker
              note
              mode="dropdown"
              selectedValue={this.state.category}
              option={this.state.arrCategoriesCash}
              error={this.state.categoryError}
              //  iosIcon={<Icon name="arrow-down" />}
              onValueChange={this.onChangeText('category')}
            />
          </Row>
        </>
      );
    } else {
      return <></>;
    }
  };

  renderPaymentImage = () => {
    if (this.state.formType == 0 && this.state.paymentImage != null) {
      return (
        <>
          <View style={FormStyle.fileSection}>
            <View>
              <FileItem
                pressHandler={this.fileHandler}
                viewFileHandler={this.viewFileHandler}
                fileData={this.state.paymentImage}
                index={0}
                type={0}
                disabled={false}></FileItem>
            </View>
          </View>
        </>
      );
    } else if (
      this.state.formType == 1 &&
      this.state.paymentPreviewImage != null
    ) {
      return (
        <>
          <View style={FormStyle.fileSection}>
            <View>
              <FileItem
                pressHandler={this.fileHandler}
                viewFileHandler={this.viewFileHandler}
                fileData={this.state.paymentPreviewImage}
                index={0}
                type={0}
                disabled={false}></FileItem>
            </View>
          </View>
        </>
      );
    } else if (this.state.formType == 1 && this.state.paymentImage != null) {
      return (
        <>
          <View style={FormStyle.fileSection}>
            <View>
              <FileItem
                pressHandler={this.fileHandler}
                viewFileHandler={this.viewFileHandler}
                fileData={this.state.paymentImage}
                index={0}
                type={0}
                disabled={false}></FileItem>
            </View>
          </View>
        </>
      );
    } else {
      return <></>;
    }
  };

  renderAddPayment = () => {
    return (
      <>
        <Content
          padder
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'space-between',
          }}
          enabled>
          {/* <KeyboardAvoidingView style={{flex:1}}  behavior={Platform.select({ android: null, ios: "padding" })} enabled>
          <ScrollView
            contentContainerStyle={{
              flex: 1
            }}
            showsHorizontalScrollIndicator={false}
            
          > */}
          <Grid>
            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Date*'} />
              <EADatePicker
                defaultDate={new Date()}
                minimumDate={new Date(2016, 1, 1)}
                maximumDate={new Date()}
                locale={'en'}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={'fade'}
                androidMode={'default'}
                onDateChange={this.onChangeText('date')}
                disabled={false}
                formatChosenDate={date => {
                  return utility.formatDate(date);
                }}
              />
            </Row>

            <Row style={styles.InputSection}>
              <EATextLabel labelText={'Payment Type*'} />

              <EAPicker
                note
                mode="dropdown"
                selectedValue={this.state.typeId}
                option={Enums.paymentType}
                error={this.state.typeIdError}
                //  iosIcon={<Icon name="arrow-down" />}
                onValueChange={this.onChangeText('typeId')}
              />
            </Row>

            {this.state.typeId == 3
              ? this.renderSupplierPayment()
              : this.renderCategory()}

            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Payment Amount*'} />
              <EATextInput
                autoCapitalize="none"
                value={this.state.paymentAmount}
                keyboardType="number-pad"
                error={this.state.paymentAmountError}
                onBlur={this.onBlurText(
                  'numeric',
                  'paymentAmountError',
                  'paymentAmount',
                )}
                onChangeText={this.onChangeText('paymentAmount')}
              />
            </Row>
            <Row style={styles.InputSection}>
              <EATextLabel labelText={'Mode*'} />
              <EAPicker
                note
                mode="dropdown"
                selectedValue={this.state.mode}
                option={this.state.arrPaymentModes}
                error={this.state.modeError}
                //  iosIcon={<Icon name="arrow-down" />}
                onValueChange={this.onChangeText('mode')}
              />
            </Row>
            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Description'} />
              <EATextInput
                autoCapitalize="none"
                value={this.state.description}
                keyboardType="default"
                onChangeText={this.onChangeText('description')}
              />
            </Row>
            <Row style={FormStyle.fileInputSection}>
              <Button dark transparent activeOpacity={1}>
                <Text style={FormStyle.inputLabel}>Attachment</Text>
              </Button>
              <Button
                onPress={this.showOption}
                style={FormStyle.attachButton}
                dark
                transparent>
                <Icon name="ios-add" style={FormStyle.attachIcon} />
              </Button>
            </Row>
          </Grid>
          {/* </ScrollView>
        </KeyboardAvoidingView> */}
          {this.renderPaymentImage()}
        </Content>
        <Footer>
          <FooterTab>
            <Button full onPress={this.submitPaymentForm}>
              <Text>{this.state.formType == 0 ? 'Add Payment' : 'Update'}</Text>
            </Button>
          </FooterTab>
        </Footer>
      </>
    );
  };

  renderPaymentBigImage() {
    var images = [
      {
        url: this.state.fullScreenImage,
        freeHeight: true,
      },
    ];
    return (
      <Modal
        visible={this.state.showPaymentImage}
        transparent={true}
        onRequestClose={() =>
          this.setState({
            showPaymentImage: false,
            fullScreenImage: null,
            imageIndex: 0,
          })
        }>
        <ImageViewer
          imageUrls={images}
          index={this.state.imageIndex}
          onSwipeDown={() => {
            this.setState({
              showPaymentImage: false,
              fullScreenImage: null,
              imageIndex: 0,
            });
          }}
          onMove={data => console.log(data)}
          enableSwipeDown={true}
        />
      </Modal>
    );
  }

  render() {
    return (
      <>
        <StyleProvider style={getTheme(commonColors)}>
          <Container>
            <Header noShadow>
              <Left>
                <Button
                  transparent
                  onPress={() => this.props.navigation.goBack()}>
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              <Body>
                <Title style={FormStyle.headerColor}>
                  {this.state.formType == 0 ? 'Add Payment' : 'Edit Payment'}
                </Title>
              </Body>
              <Right />
            </Header>
            {this.state.isLoading ? <Loader /> : this.renderAddPayment()}
          </Container>
        </StyleProvider>
        {this.renderPaymentBigImage()}
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  headerColor: ToolbarHeader,
  dropdown: {flexDirection: 'row', position: 'relative'},
  InputSection: {
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    paddingBottom: 15,
  },
});

export default AddPaymentScreen;
