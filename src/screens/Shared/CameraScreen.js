import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Button,
  Right,
  Body,
  Icon,
  Text,
  List,
  ListItem,
  Item,
  Input
} from "native-base";
// import { Camera } from "expo-camera";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import Loader from "./Loader";
class CameraScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      cameraRatio: "16:9",
      refreshing: false
    };
  }

  async componentDidMount() {}
}

const styles = StyleSheet.create({
  camerIcon: { color: "#fff", fontSize: 30 },
  cameraButton: {
    alignSelf: "flex-end",
    alignItems: "center"
  },
  buttonGroup: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 20,
  }
});

export default CameraScreen;
