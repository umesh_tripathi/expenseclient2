import React from 'react';
import{
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,
  Modal
} from "react-native";
import getTheme from "../../../native-base-theme/components";
import commonColors from "../../../native-base-theme/variables/commonColor";
import {Icon,StyleProvider,Button} from "native-base";

class ImageViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
      }

  render() {
      const {image,closeHandler} = this.props;
    return (
        <>
        <StyleProvider style={getTheme(commonColors)}>
         <View style={styles.container}>
          <View style={styles.fullscreenImage}>
            <Image
              resizeMode="contain"
              source={{ uri: image }}
              style={{ flex: 1, marginTop:30,
                marginBottom:30 }}
            ></Image>
            <Button
            transparent
              style={styles.overlayCancel}
              onPress={() => {
                closeHandler()
              }}
            >
              <Icon type="Ionicons" name="ios-close" style={styles.cancelIcon} size={20} />
            </Button>
          </View>
      </View>
      </StyleProvider>
      </>
    );
  }
}

const styles = StyleSheet.create({
    container:{ flex: 1 ,backgroundColor: "#000000"},
  overlayCancel: {
    padding: 20,
    position: "absolute",
    right: 10,
    top: 0
  },
  fullscreenImage:{
    flex: 1,
  },
  modal: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#000000"
  },
  cancelIcon: {
    color: "white"
  }
});

export default ImageViewer;
