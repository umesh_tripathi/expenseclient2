import React, {Component} from 'react';

import {Platform, PermissionsAndroid} from 'react-native';
import {
  Container,
  Header,
  Content,
  Button,
  Right,
  Body,
  Icon,
  Text,
  List,
  ListItem,
  Item,
  Input,
} from 'native-base';
import Contacts from 'react-native-contacts';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Loader from './Loader';
class ContactsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      contacts: [],
      refreshing: false,
    };
  }

  componentDidMount() {
    this.setState({loading: true});
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: 'Contacts',
        message: 'fingerchips app would like to view your contacts.',
      }).then(() => {
        this.loadContacts();
      });
    } else {
      this.loadContacts();
    }
  }

  loadContacts = async () => {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        this.setState({contacts: [], inMemoryContacts: [], loading: false});
      } else {
        this.setState({
          contacts: contacts,
          inMemoryContacts: contacts,
          loading: false,
        });
      }
    });
  };

  searchContacts = value => {
    const filteredContacts = this.state.inMemoryContacts.filter(contact => {
      let contactLowercase = contact.displayName.toLowerCase();

      let searchTermLowercase = value.toLowerCase();

      return contactLowercase.indexOf(searchTermLowercase) > -1;
    });
    this.setState({contacts: filteredContacts});
  };

  renderContact = item => {
    if(item != undefined && item.phoneNumbers != undefined && item.phoneNumbers != undefined){
      if(item.phoneNumbers.length !=0 ){
        var phoneNumber = item.phoneNumbers[0]
        return( <><Text> {item.displayName}</Text><Text note>{phoneNumber.number} </Text></>)   
      }else{
        return(<></>)
      }
         
    }else{
      return(<></>)
    }
  };

  renderItem = (item, pressHandler) => {
    if(item != undefined && item.phoneNumbers != undefined && item.phoneNumbers != undefined){
      if(item.phoneNumbers.length !=0 ){
        var phoneNumber = item.phoneNumbers[0]
        return(   <ListItem
          onPress={() => this.props.pressHandler(1, item)}
          key={item.recordID + ''}>
          <Body>
          <Text>{item.displayName}</Text><Text note>{phoneNumber.number} </Text>
          </Body>
          <Right>
            <Icon name="arrow-forward" />
          </Right>
        </ListItem>)   
      }else{
        return(<></>)
      }
    }else{
      return(<></>)
    }
  }

  render() {
    const {pressHandler} = this.props;
    return (
      <Container>
        <Header searchBar rounded>
          <Button transparent onPress={() => pressHandler(0, {})}>
            <Text>Back</Text>
          </Button>
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="Search"
              onChangeText={value => this.searchContacts(value)}
            />
            <Icon name="ios-people" />
          </Item>
        </Header>

        {this.state.loading == true ? (
          <Loader />
        ) : (
          <Content>
            <List>
              {this.state.contacts.map((value, index) => {
                return this.renderItem(value, pressHandler);
              })}
            </List>
          </Content>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({});

export default ContactsScreen;
