import React, {Component} from 'react';
import {
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
  Keyboard,
  View,
  Switch,
  ProgressBarAndroid,
  ProgressViewIOS,
  ToastAndroid,
  PermissionsAndroid,
} from 'react-native';
import {
  Container,
  Content,
  Button,
  ListItem,
  Body,
  Text,
  StyleProvider,
  Grid,
  Toast,
  Row,
  Col,
  Spinner,
} from 'native-base';
import RNFetchBlob from 'rn-fetch-blob';
import getTheme from '../../../native-base-theme/components';
import commonColors from '../../../native-base-theme/variables/commonColor';
import {EATextLabel, EADatePicker, RadioButton} from '../../components';
import {isValid, utility, userPreferences} from '../../utility';
import UserService from '../../services/user';
import {zip, unzip, unzipAssets, subscribe} from 'react-native-zip-archive';
class ExportFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: 'Current Month',
      fromDate: new Date(),
      toDate: new Date(),
      fromDateError: '',
      toDateError: '',
      progress: 0,
      downloadCount: 0,
      includeImages: false,
      loading: false,
      radioItems: [
        {
          label: 'Current Month',
          color: '#FE3852',
          size: 25,
          id: 1,
          selected: true,
        },
        {
          label: 'Last Month',
          size: 25,
          color: '#FE3852',
          id: 2,
          selected: false,
        },
        {
          label: 'Custom',
          size: 25,
          color: '#FE3852',
          id: 3,
          selected: false,
        },
      ],
    };
    this.submitExport = this.submitExport.bind(this);
    this.excelDownload = this.excelDownload.bind(this);
  }

  componentDidMount() {
    this.state.radioItems.map(item => {
      if (item.selected == true) {
        this.setState({selectedItem: item.label});
      }
    });
  }

  _onChangeText = key => text => {
    this.setState({
      [key]: text,
    });
  };

  _onBlurText = (validatorKey, errorKey, stateKey) => () => {
    this.setState({
      [errorKey]: isValid(validatorKey, this.state[stateKey]),
    });
  };

  changeActiveRadioButton(index) {
    this.state.radioItems.map(item => {
      item.selected = false;
    });

    this.state.radioItems[index].selected = true;

    this.setState({radioItems: this.state.radioItems}, () => {
      this.setState({selectedItem: this.state.radioItems[index].label});
    });
  }

  submitExport = async () => {
    if (this.state.selectedItem == 'Custom') {
      this.setState({
        loading: true,
      });
      let fromDateError = isValid('required', this.state.fromDate);
      let toDateError = isValid('required', this.state.toDate);

      this.setState({
        fromDateError,
        toDateError,
      });

      if (!fromDateError && !toDateError) {
        if (this.state.toDate < this.state.fromDate) {
          Keyboard.dismiss();
          Toast.show({
            text: 'Please select appropriate dates.',
            buttonText: 'Ok',
            type: 'danger',
            duration: 5000,
          });

          this.setState({
            loading: false,
          });
          return;
        } else {
          this.setState({
            loading: false,
          });
        }
      }
      this.setState({
        loading: false,
      });
    }

    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission',
            message: 'App needs access to phone storage to download the file.',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.getExportData();
        } else {
          Toast.show({
            text: 'You need to give storage permission to download the file.',
            buttonText: 'Ok',
            type: 'success',
            duration: 5000,
          });
        }
      } catch (err) {
        console.log(err);
      }
    } else {
      this.getExportData();
    }
  };

  getExportData = async () => {
    try {
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      let userShopId = await userPreferences.getPreferences(
        userPreferences.userShopId,
      );
      var filterType = 0;

      if (this.state.selectedItem == 'Custom') {
        filterType = 2;
      } else if (this.state.selectedItem == 'Last Month') {
        filterType = 1;
      }

      var formData = {
        filterType: filterType,
        fromDate: this.state.fromDate,
        toDate: this.state.toDate,
        shopId: userShopId,
        userId: userId,
        supplier_id: null,
        category_id: null,
        image_inculde: this.state.includeImages == true ? 1 : 0,
      };
      this.setState({
        downloadCount: 0,
        loading: true,
      });
      let userExcelReport = await UserService.userExcelReport(formData);
      this.setState({loading: false, progress: 0});

      if (userExcelReport.status == 0) {
        var msg = userExcelReport.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else {
        let shopName = await userPreferences.getPreferences(
          userPreferences.userShopName,
        );

        if (shopName != null && shopName != '') {
          shopName = utility.titleCase(shopName);
        }
        var date = new Date();
        let dirs = RNFetchBlob.fs.dirs;
        var ext = utility.extention(userExcelReport.Url);
        ext = '.' + ext[0];

        var timeStamp = Math.floor(date.getTime() + date.getSeconds() / 2);
        var filename = 'report' + timeStamp + ext;
        var rootFolderName = 'ExpenseApp-' + shopName + '-' + timeStamp;
        var localUrlPath =
          Platform.OS === 'android'
            ? dirs.DownloadDir + '/' + rootFolderName + '/' + filename
            : dirs.DocumentDir + '/' + rootFolderName + '/' + filename;

        if (this.state.includeImages == true) {
          if (userExcelReport.image_array.length > 0) {
            this.excelDownload(
              userExcelReport.Url,
              localUrlPath,
              filename,
              false,
            );
            var that = this;
            this.setState(
              {
                progress: 0,
                downloadCount: userExcelReport.image_array.length,
              },
              () => {
                userExcelReport.image_array.forEach(function(value, key) {
                  var imgExt = utility.extention(value.billPath);
                  imgExt = '.' + imgExt[0];
                  var imgFilename = value.billImage + imgExt;
                  var localImagePath =
                    Platform.OS === 'android'
                      ? dirs.DownloadDir +
                        '/ExpenseApp-' +
                        shopName +
                        '-' +
                        timeStamp
                      : dirs.DocumentDir +
                        '/ExpenseApp-' +
                        shopName +
                        '-' +
                        timeStamp;
                  if (userExcelReport.image_array.length == key + 1) {
                    that.downloadImages(
                      value.billPath,
                      localImagePath,
                      imgFilename,
                      imgExt,
                      true,
                      key + 1,
                      rootFolderName,
                    );
                  } else {
                    that.downloadImages(
                      value.billPath,
                      localImagePath,
                      imgFilename,
                      imgExt,
                      false,
                      key + 1,
                      rootFolderName,
                    );
                  }
                });
              },
            );
          } else {
            this.excelDownload(
              userExcelReport.Url,
              localUrlPath,
              filename,
              true,
            );
          }
        } else {
          this.excelDownload(userExcelReport.Url, localUrlPath, filename, true);
        }
      }
    } catch (error) {
      this.setState({loading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  excelDownload = async (fileUrl, localUrlPath, filename, openType) => {
    this.setState({
      loading: true,
    });
    const android = RNFetchBlob.android;

    let downloadOptions = {
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        title: '' + filename,
        description: 'Downloading file.',
        mediaScannable: true,
        mime:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        path: localUrlPath,
      },
      path: localUrlPath,
    };

    RNFetchBlob.config(downloadOptions)
      .fetch('GET', fileUrl)
      .progress((received, total) => {})
      .then(res => {
        this.setState({
          loading: false,
        });

        if (openType == true) {
          Toast.show({
            text: 'Your file has been downloaded to downloads folder.',
            buttonText: 'Ok',
            type: 'success',
            duration: 5000,
          });
          if (Platform.OS === 'android') {
            this.props.completionHandler();
            android.actionViewIntent(
              localUrlPath,
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            );
          } else {
            this.props.completionHandler();
            // RNFetchBlob.ios.previewDocument(localUrlPath);
          }
        }
      })
      .catch(function(error) {
        this.setState({
          loading: false,
        });
      });
  };

  downloadImages = async (
    fileUrl,
    localUrlPath,
    filename,
    extension,
    openType,
    filecount,
    rootFolderName,
  ) => {
    this.setState({
      loading: true,
    });
    const android = RNFetchBlob.android;
    let downloadOptions = {
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        title: '' + filename,
        description: 'Downloading file.',
        mediaScannable: true,
        mime: 'image/png',
        path: localUrlPath + '/Images/' + filename,
      },
      path: localUrlPath + '/Images/' + filename,
    };
    let dirs = RNFetchBlob.fs.dirs;
    let fileManager = RNFetchBlob.fs;
    //vnd.android.document/directory
    RNFetchBlob.config(downloadOptions)
      .fetch('GET', fileUrl)
      .progress((received, total) => {})
      .then(res => {
        var that = this;
        this.setState({
          progress: filecount * (1 / that.state.downloadCount),
          loading: false,
        });

        if (Platform.OS === 'android') {
          if (openType == true) {
            var sourcePath = localUrlPath;
            var targetPath =
              Platform.OS === 'android'
                ? dirs.DownloadDir + '/' + rootFolderName + '.zip'
                : dirs.DocumentDir + '/' + rootFolderName + '.zip';

            that.setState({
              loading: true,
            });
            zip(sourcePath, targetPath)
              .then(path => {
                that.setState({
                  loading: false,
                });
                fileManager
                  .unlink(sourcePath)
                  .then(() => {
                    that.setState({
                      loading: false,
                    });
                    android.actionViewIntent(targetPath, 'application/zip');
                  })
                  .catch(err => {
                    that.setState({
                      loading: false,
                    });
                    console.log('Unlink err : ', err);
                  });
              })
              .catch(error => {
                that.setState({
                  loading: false,
                });
              });
            this.props.completionHandler();
          }
        } else if (openType == true) {
          Toast.show({
            text: 'Your file has been downloaded.You can view it in Files app.',
            buttonText: 'Ok',
            type: 'success',
            duration: 5000,
          });
          var sourcePath = localUrlPath;
          var targetPath =
            Platform.OS === 'android'
              ? dirs.DownloadDir + '/' + rootFolderName + '.zip'
              : dirs.DocumentDir + '/' + rootFolderName + '.zip';

          that.setState({
            loading: true,
          });
          zip(sourcePath, targetPath)
            .then(path => {
              that.setState({
                loading: false,
              });
              fileManager
                .unlink(sourcePath)
                .then(() => {
                  that.setState({
                    loading: false,
                  });
                  console.log('Unlink Success : ');
                })
                .catch(err => {
                  that.setState({
                    loading: false,
                  });
                  console.log('Unlink err : ', err);
                });
            })
            .catch(error => {
              that.setState({
                loading: false,
              });
            });
          this.props.completionHandler();
        }
      })
      .catch(function(error) {
        this.setState({
          loading: false,
        });
      });
  };

  renderProgress() {
    if (this.state.progress != 0) {
      return (
        <View style={styles.progressGroup}>
          {Platform.OS === 'android' ? (
            <ProgressBarAndroid
              styleAttr="Horizontal"
              color="#FE3852"
              progress={this.state.progress}
              indeterminate={false}
            />
          ) : (
            <ProgressViewIOS
              progress={this.state.progress}
              progressTintColor="#FE3852"
            />
          )}
          <EATextLabel
            labelText={'Please wait while we are downloading the file.'}
          />
        </View>
      );
    } else {
      return <></>;
    }
  }

  render() {
    const {completionHandler} = this.props;
    return (
      <>
        <StyleProvider style={getTheme(commonColors)}>
          <KeyboardAvoidingView
            style={styles.container}
            behavior={Platform.select({android: null, ios: 'padding'})}
            enabled>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={styles.headerLabel}>Export Summary</Text>
                {this.state.loading ? (
                  <Spinner color="red" style={{height: 20}} />
                ) : (
                  <></>
                )}
              </View>

              <View style={styles.radioGroup}>
                {this.state.radioItems.map((item, key) => (
                  <RadioButton
                    key={key}
                    button={item}
                    onClick={this.changeActiveRadioButton.bind(this, key)}
                  />
                ))}
              </View>

              {this.state.selectedItem == 'Custom' ? (
                <View style={styles.dateForm}>
                  <View>
                    <EATextLabel labelText={'From Date'} />
                    <EADatePicker
                      defaultDate={new Date()}
                      minimumDate={new Date(2016, 1, 1)}
                      maximumDate={new Date()}
                      locale={'en'}
                      timeZoneOffsetInMinutes={undefined}
                      modalTransparent={false}
                      animationType={'fade'}
                      androidMode={'default'}
                      onDateChange={this._onChangeText('fromDate')}
                      disabled={false}
                      formatChosenDate={date => {
                        return utility.formatDate(date);
                      }}
                    />
                  </View>
                  <View>
                    <EATextLabel labelText={'To Date'} />
                    <EADatePicker
                      defaultDate={new Date()}
                      minimumDate={new Date(2016, 1, 1)}
                      maximumDate={new Date()}
                      locale={'en'}
                      timeZoneOffsetInMinutes={undefined}
                      modalTransparent={false}
                      animationType={'fade'}
                      androidMode={'default'}
                      onDateChange={this._onChangeText('toDate')}
                      disabled={false}
                      formatChosenDate={date => {
                        return utility.formatDate(date);
                      }}
                    />
                  </View>
                </View>
              ) : (
                <></>
              )}
              <View style={styles.checkBox}>
                <Switch
                  value={this.state.includeImages}
                  onValueChange={this._onChangeText('includeImages')}
                  disabled={this.state.loading}
                />
                <Text style={styles.checkBoxLabel}> Include Bill Images</Text>
              </View>

              {this.renderProgress()}

              <View style={styles.buttonGroup}>
                <Button
                  style={styles.cancelButton}
                  transparent
                  disabled={this.state.loading}
                  onPress={() => completionHandler()}>
                  <Text style={styles.cancelLabel}>Cancel</Text>
                </Button>
                <Button
                  style={styles.saveButton}
                  transparent
                  disabled={this.state.loading}
                  onPress={this.submitExport}>
                  <Text style={styles.saveLabel}>Ok</Text>
                </Button>
              </View>
            </View>
          </KeyboardAvoidingView>
        </StyleProvider>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
    width: '80%',
    borderColor: 'rgba(0, 0, 0, 0.4)',
  },
  dateForm: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
  },
  progressGroup: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 10,
  },
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 2,
    marginTop: 10,
    marginBottom: 10,
  },
  checkBoxLabel: {
    color: '#637381',
    fontSize: 15,
    fontFamily: 'Roboto-Medium',
  },
  radioGroup: {
    alignItems: 'flex-start',
  },
  headerLabel: {
    alignItems: 'flex-start',
    color: '#FE3852',
    margin: 10,
    fontSize: 18,
    fontWeight: '400',
  },
  amountGroup: {
    margin: 5,
  },
  buttonGroup: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    margin: 10,
    alignItems: 'flex-end',
  },
  cancelButton: {
    marginRight: 10,
  },
  cancelLabel: {
    alignSelf: 'center',
    color: '#637381',
    fontSize: 18,
    fontWeight: '400',
  },
  saveButton: {
    marginLeft: 10,
  },
  saveLabel: {
    alignSelf: 'center',
    color: '#FE3852',
    fontSize: 18,
    fontWeight: '400',
  },
  contentTitle: {
    fontSize: 20,
    marginBottom: 12,
  },
});

export default ExportFilter;
