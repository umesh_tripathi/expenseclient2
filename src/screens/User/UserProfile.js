import React, {Component} from 'react';
import {
  StyleSheet,
  Image,
  Platform,
  AsyncStorage,
  BackHandler,
  Modal,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Col,
  Grid,
  Row,
  Thumbnail,
  View,
  H1,
  Toast,
  ActionSheet,
  StyleProvider,
} from 'native-base';
import ImageViewer from 'react-native-image-zoom-viewer';
import ImagePicker from 'react-native-image-picker';
import getTheme from '../../../native-base-theme/components';
import commonColors from '../../../native-base-theme/variables/commonColor';
import {ToolbarHeader, FormStyle} from '../../styles';
import {
  isValid,
  userPreferences,
  utility,
  createFormData,
  ProgressiveImage
} from '../../utility';
import Loader from '../Shared/Loader';
import AppConstant from '../../utility/AppConstant';
import UserService from '../../services/user';
import {EATextInput, EATextLabel} from '../../components';
const imageOptions = {
  title: 'Select Image',
  quality: 0.5,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class UserProfile extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      firstName: '',
      firstNameError: '',
      lastName: '',
      lastNameError: '',
      email: '',
      emailError: '',
      phone: '',
      phoneError: '',
      isProfileEdit: false,
      hasCameraPermission: null,
      profileInfo: null,
      profileImage: null,
      subscriptionEndDate: null,
      profileImageObject: null,
      isImageVisible: false,
    };
    this.firstName = React.createRef();
    this.lastName = React.createRef();
    this.email = React.createRef();
    this.phone = React.createRef();
    this.editProfile = this.editProfile.bind(this);
    this.showProfileOption = this.showProfileOption.bind(this);
    
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this.getProfile();
  }

  async componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    return false;
  };

  _onChangeText = key => text => {
    this.setState({
      [key]: text,
    });
  };

  _onBlurText = (validatorKey, errorKey, stateKey) => () => {
    this.setState({
      [errorKey]: isValid(validatorKey, this.state[stateKey]),
    });
  };

  showProfileOption = async () =>{

    if (
      this.state.profileImage == null ||
      this.state.profileImage == '' ||
      this.state.profileImage == 'null'
    ) {
      this.showOption()
    }else{
      var BUTTONS = ["Select profile picture", "Remove profile picture", "Cancel"];
      var CANCEL_INDEX = 2;
      ActionSheet.show(
        {
          options: BUTTONS,
          cancelButtonIndex: CANCEL_INDEX,
          title: "Choose option"
        },
        buttonIndex => {
          if(buttonIndex == 0){
            this.showOption()
          }else  if(buttonIndex == 1){
            this.setState({
              profileImage: null,
              profileImageObject: null
            },()=>{
              this.updateProfile(0)
            })
          }else{
          }
        }
      )
    }
    
  }

  showOption = async () => {
    ImagePicker.showImagePicker(imageOptions, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else {
        this.setState(
          {profileImage: response.uri, profileImageObject: response},
          () => {
            this.updateProfile(0);
          },
        );
      }
    });
  };

  validate = async () => {
    let status = {valid: true, message: ''};
    let firstNameError = isValid('firstName', this.state.firstName);
    let lastNameError = isValid('lastName', this.state.lastName);
    let emailError = isValid('email', this.state.email);
    let phoneError = isValid('phone', this.state.phone);

    let promise = new Promise((resolve, reject) => {
      this.setState(
        {
          firstNameError,
          lastNameError,
          emailError,
          phoneError,
        },
        () => {
          if (this.state.firstNameError) {
            status.valid = false;
            status.message = firstNameError;
          } else if (this.state.lastNameError) {
            status.valid = false;
            status.message = lastNameError;
          } else if (this.state.emailError) {
            status.valid = false;
            status.message = emailError;
          } else if (this.state.phoneError) {
            status.valid = false;
            status.message = phoneError;
          }
          resolve(status);
        },
      );
    });

    return promise;
  };

  updateProfile = async type => {
    if (this.state.profileInfo == null) {
      return;
    }
    try {
      if (type == 1) {
        let status = await this.validate();
        if (!status.valid) {
          Toast.show({
            text: `${status.message}!`,
            buttonText: 'Ok',
            position: 'bottom',
            type: 'danger',
            duration: 5000,
          });
          return;
        }
      }

      this.setState({isLoading: true});

      let userId = await userPreferences.getPreferences(userPreferences.userId);

      var formData = {
        first_name:
          type == 0 ? this.state.profileInfo.first_name : this.state.firstName,
        last_name:
          type == 0 ? this.state.profileInfo.last_name : this.state.lastName,
        phone_number:
          type == 0 ? this.state.profileInfo.phone_number : this.state.phone,
        email_id:
          type == 0 ? this.state.profileInfo.email_id : this.state.email,
        address:
          this.state.profileInfo.address == undefined
            ? ''
            : this.state.profileInfo.address,
        business_name: this.state.profileInfo.business_name,
        id: userId,
        profile_photo:
          this.state.profileImage != null ? this.state.profileImage : '',
      };


      let multipartData = formData;
      if (this.state.profileImageObject != null) {
        multipartData = await createFormData(
          'profile_photo',
          this.state.profileImageObject,
          formData,
          false,
        );
      }

      let serverCallUser = await UserService.updateProfile(
        multipartData,
        formData.id,
      );
      this.setState({isLoading: false});
      if (serverCallUser.status == 0) {
        var msg = serverCallUser.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'success',
          duration: 5000,
        });
      } else {
        this.getProfile();
        var msg = serverCallUser.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'success',
          duration: 5000,
        });
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text:
            error && error.message
              ? error.message
              : error || 'Not Valid Error!',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  getProfile = async () => {
    try {
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      this.setState({isLoading: true});
      let userData = await UserService.getProfile(userId);
      this.setState({isLoading: false});

      if (userData.status == 0) {
        var msg = userData.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'success',
          duration: 5000,
        });
      } else {
        if (userData.userInfo != null) {
          this.setState({
            profileInfo: userData.userInfo,
            subscriptionEndDate:
              userData.end_date != '' ? userData.end_date : null,
            profileImage:
              userData.userInfo.profile_photo != null
                ? userData.userInfo.profile_photo
                : null,
            firstName: utility.titleCase(userData.userInfo.first_name),
            lastName: utility.titleCase(userData.userInfo.last_name),
            email: userData.userInfo.email_id,
            phone: userData.userInfo.phone_number,
            firstNameError: '',
            lastNameError: '',
            emailError: '',
            phoneError: '',
            isProfileEdit: false,
          });
          userPreferences.setPreferences(
            userPreferences.profilePhoto,
            userData.userInfo.profile_photo != null
              ? userData.userInfo.profile_photo + ''
              : '',
          );

          userPreferences.setPreferences(
            userPreferences.firstName,
            userData.userInfo.first_name,
          );

          userPreferences.setPreferences(
            userPreferences.lastName,
            userData.userInfo.last_name,
          );

          userPreferences.setPreferences(
            userPreferences.emailId,
            userData.userInfo.email_id,
          );

          userPreferences.setPreferences(
            userPreferences.phoneNumber,
            userData.userInfo.phone_number,
          );
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  editProfile() {
    this.setState({
      isProfileEdit: !this.state.isProfileEdit,
      firstNameError: '',
      lastNameError: '',
      emailError: '',
      phoneError: '',
    });
  }

  viewImage(image) {
    this.setState({isImageVisible: true});
  }

  renderProfileBigImage() {
    var images = [
      {
        url: this.state.profileImage,
        freeHeight: true,
      },
    ];
    return (
      <Modal
        visible={this.state.isImageVisible}
        transparent={true}
        onRequestClose={() => this.setState({isImageVisible: false})}>
        <ImageViewer
          imageUrls={images}
          index={this.state.index}
          onSwipeDown={() => {
            this.setState({isImageVisible: false});
          }}
          onMove={data => console.log(data)}
          enableSwipeDown={true}
        />
      </Modal>
    );
  }

  renderProfileImage() {
    if (
      this.state.profileImage == null ||
      this.state.profileImage == '' ||
      this.state.profileImage == 'null'
    ) {
      return (
        <TouchableOpacity
          onPress={() => {
            // this.viewImage(AppConstant.ImageConstant.ic_silhouette);
          }}
          activeOpacity={1}>
          <Thumbnail
            circle
            large
            source={AppConstant.ImageConstant.ic_silhouette}
            style={styles.profileImage}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => {
            this.viewImage(this.state.profileImage);
          }}>
          <Thumbnail
            circle
            large
            source={{uri: this.state.profileImage}}
            style={styles.profileImage}
          />
        </TouchableOpacity>
      );
    }
  }

  renderProfileType = () => {
    if (this.state.isProfileEdit) {
      return (
        <Grid>
          <Row style={FormStyle.InputSection}>
            <EATextLabel labelText={'First Name*'} />
            <EATextInput
              autoCapitalize="words"
              value={this.state.firstName}
              placeholder="First Name"
              onChangeText={this._onChangeText('firstName')}
              error={this.state.firstNameError}
              onBlur={this._onBlurText(
                'firstName',
                'firstNameError',
                'firstName',
              )}
              returnKeyType={'next'}
              ref={this.firstName}
              onSubmitEditing={() => this.lastName.current.focusInput()}
            />
          </Row>
          <Row style={FormStyle.InputSection}>
            <EATextLabel labelText={'Last Name*'} />
            <EATextInput
              autoCapitalize="words"
              value={this.state.lastName}
              placeholder="Last Name"
              onChangeText={this._onChangeText('lastName')}
              error={this.state.lastNameError}
              onBlur={this._onBlurText('lastName', 'lastNameError', 'lastName')}
              returnKeyType={'next'}
              ref={this.lastName}
              onSubmitEditing={() => this.email.current.focusInput()}
            />
          </Row>
          <Row style={FormStyle.InputSection}>
            <EATextLabel labelText={'Email *'} />
            <EATextInput
              autoCapitalize="none"
              autoCompleteType="email"
              value={this.state.email}
              keyboardType="email-address"
              placeholder="Email"
              onChangeText={this._onChangeText('email')}
              error={this.state.emailError}
              onBlur={this._onBlurText('email', 'emailError', 'email')}
              returnKeyType={'next'}
              ref={this.email}
              onSubmitEditing={() => this.phone.current.focusInput()}
            />
          </Row>
          <Row style={FormStyle.InputSection}>
            <EATextLabel labelText={'Phone*'} />
            <EATextInput
              autoCapitalize="none"
              autoCompleteType="tel"
              value={this.state.phone}
              keyboardType="number-pad"
              placeholder="Phone Number"
              onChangeText={this._onChangeText('phone')}
              error={this.state.phoneError}
              onBlur={this._onBlurText('phone', 'phoneError', 'phone')}
              returnKeyType={'next'}
              ref={this.phone}
              onSubmitEditing={() => this.phone.current.blurInput()}
            />
          </Row>
        </Grid>
      );
    } else {
      return (
        <>
          <Grid style={styles.item}>
            <Row style={styles.row}>
              <Col size={30} style={styles.leftCol}>
                <Row>
                  <Text style={styles.leftTitle}>Name</Text>
                </Row>
              </Col>
              <Col size={70} style={styles.rightCol}>
                <Row>
                  <Text style={styles.userName}>
                    {this.state.profileInfo.first_name}{' '}
                    {this.state.profileInfo.last_name}
                  </Text>
                </Row>
              </Col>
            </Row>
          </Grid>
          <Grid style={styles.item}>
            <Row style={styles.row}>
              <Col size={40} style={styles.leftCol}>
                <Row>
                  <Text style={styles.leftTitle}>Email Address</Text>
                </Row>
              </Col>
              <Col size={60} style={styles.rightCol}>
                <Row>
                  <Text style={styles.rightTitle}>
                    {this.state.profileInfo.email_id}
                  </Text>
                </Row>
              </Col>
            </Row>
          </Grid>
          <Grid style={styles.item}>
            <Row style={styles.row}>
              <Col size={30} style={styles.leftCol}>
                <Row>
                  <Text style={styles.leftTitle}>Mobile No</Text>
                </Row>
              </Col>
              <Col size={70} style={styles.rightCol}>
                <Row>
                  <Text style={styles.rightTitle}>
                    {this.state.profileInfo.phone_number}
                  </Text>
                </Row>
              </Col>
            </Row>
          </Grid>
          {this.state.subscriptionEndDate != null ? (
            <Grid style={styles.item}>
              <Row style={styles.row}>
                <Col size={50} style={styles.leftCol}>
                  <Row>
                    <Text style={styles.leftTitle}>Subscription End Date</Text>
                  </Row>
                </Col>
                <Col size={50} style={styles.rightCol}>
                  <Row>
                    <Text style={styles.rightTitle}>
                      {this.state.subscriptionEndDate}
                    </Text>
                  </Row>
                </Col>
              </Row>
            </Grid>
          ) : (
            <Grid style={styles.item}>
              <Row style={styles.row}>
                <Col style={styles.leftCol}>
                  <Row>
                    <Text style={styles.leftTitle}>
                      Your subscription has been expired.
                    </Text>
                  </Row>
                </Col>
              </Row>
            </Grid>
          )}
        </>
      );
    }
  };

  renderProfile = () => {
    if (this.state.profileInfo == null) {
      return (
        <View style={styles.message}>
          <H1>Oops something went wrong. Please try again.</H1>
        </View>
      );
    } else {
      return (
        <>
          {/* this.state.isProfileEdit ? styles.formContainer: */}
          <Content padder contentContainerStyle={styles.container} enabled>
            <Grid style={styles.profile}>
              <Row>
                <Col size={20}></Col>
                <Col size={60}>{this.renderProfileImage()}</Col>
                <Col size={20}>
                  <Button
                    small
                    dark
                    transparent
                    style={styles.listButton}
                    onPress={this.showProfileOption}>
                    <Icon
                      type="EvilIcons"
                      name="pencil"
                      style={styles.iconText}
                    />
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col size={60}></Col>
                <Col size={40}>
                  <Button
                    small
                    dark
                    transparent
                    style={styles.editProfile}
                    onPress={this.editProfile}>
                    <Text>
                      {this.state.isProfileEdit ? 'Cancel' : 'Edit Profile'}
                    </Text>
                  </Button>
                </Col>
              </Row>
            </Grid>
            {this.renderProfileType()}
          </Content>
          {this.state.isProfileEdit ? (
            <Footer>
              <FooterTab>
                <Button
                  full
                  onPress={() => {
                    this.updateProfile(1);
                  }}>
                  <Text>Update</Text>
                </Button>
              </FooterTab>
            </Footer>
          ) : (
            <></>
          )}
        </>
      );
    }
  };
  render() {
    return (
      <>
        <StyleProvider style={getTheme(commonColors)}>
          <Container>
            <Header noShadow>
              <Left>
                <Button
                  transparent
                  onPress={() => this.props.navigation.goBack()}>
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              <Body>
                <Title style={FormStyle.headerColor}>Profile</Title>
              </Body>
              <Right></Right>
            </Header>
            {this.state.isLoading ? <Loader /> : this.renderProfile()}
          </Container>
        </StyleProvider>
        {this.renderProfileBigImage()}
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 0,
  },
  formContainer: {
    flexGrow: 1,
  },
  profile: {
    borderBottomWidth: 1,
    borderBottomColor: '#DFDFDF',
    minHeight: 200,
    alignItems: 'center',
    marginTop: 10,
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },
  profileImage: {
    width: 140,
    height: 140,
    borderRadius: 140 / 2,
    borderWidth: 1,
    borderColor: '#DFDFDF',
    alignSelf: 'center',
  },
  item: {
    borderBottomWidth: 1,
    borderBottomColor: '#DFDFDF',
  },
  row: {
    minHeight: 50,
  },
  leftCol: {
    alignItems: 'flex-start',
    marginLeft: 10,
  },
  rightCol: {
    alignItems: 'flex-end',
    marginRight: 10,
  },
  leftTitle: {
    fontSize: 16,
    color: '#2e2e2e',
    fontWeight: '300',
    alignSelf: 'center',
  },
  rightTitle: {
    fontSize: 15,
    color: '#2e2e2e',
    fontWeight: '500',
    alignSelf: 'center',
  },
  userName: {
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#2e2e2e',
    fontWeight: '500',
    alignSelf: 'center',
  },
  listButton: {
    padding: 6,
    margin: 0,
    height: 45,
    width: 45,
    marginRight: 10,
  },
  editProfile: {
    alignSelf: 'flex-end',
    borderWidth: 2,
    borderRadius: 5,
    borderColor: '#DFDFDF',
  },
  iconText: {
    fontSize: 40,
    marginLeft: 0,
    marginRight: 0,
  },
  message: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default UserProfile;
