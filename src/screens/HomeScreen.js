import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  AsyncStorage,
  RefreshControl,
  Modal,
} from 'react-native';
import {
  Container,
  CardItem,
  Content,
  Button,
  Card,
  Icon,
  Text,
  Grid,
  Body,
  Row,
  H1,
  Toast,
  StyleProvider,
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import commonColors from '../../native-base-theme/variables/commonColor';
import {userPreferences, utility, Enums, AppConstant} from '../utility';
import {FormStyle} from '../styles';
import HomeService from '../services/home';
import Loader from './Shared/Loader';
import ExportFilter from './Shared/ExportFilter';
const SCREEN_MIN_WIDTH = Math.round((Dimensions.get('window').width - 40) / 2);
class HomeScreen extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      billOutStanding: null,
      overallSpending: null,
      paymentMade: null,
      pettyCash: null,
      otherExpense: null,
      isLoading: false,
      refreshing: false,
      filterModalVisible: false,
      fromDate: '',
      toDate: '',
      filterType: 0,
    };

    this.filterCompletionHandler = this.filterCompletionHandler.bind(this);
  }

  handleTabFocus = async () => {
    let homeTab = await userPreferences.getPreferences(userPreferences.homeTab);
    if (homeTab != null && homeTab == '1') {
      await userPreferences.setPreferences(userPreferences.homeTab, '0');
      this.getHomeDetail(0);
    }
  };


  addBillHandler = async () => {
    this.props.navigation.navigate('AddBill', {formType: 0});
  };

  addPaymentHandler = async () => {
    this.props.navigation.navigate('AddPayment', {formType: 0});
  };

  exportSummaryReportHandler = async () => {
    this.setFilterModalVisible(true);
  };

  filterCompletionHandler() {
    this.setFilterModalVisible(false);
  }

  setFilterModalVisible(visible) {
    this.setState({filterModalVisible: visible});
  }

  async componentDidMount() {
    this.getHomeDetail(0);
    this.props.navigation.addListener('didFocus', this.handleTabFocus);
  }

  getHomeDetail = async refreshType => {
    let userShopId = await userPreferences.getPreferences(
      userPreferences.userShopId,
    );

    if (userShopId == null) {
      return;
    }

    try {
      this.setState({
        billOutStanding: null,
        overallSpending: null,
        paymentMade: null,
        pettyCash: null,
        otherExpense: null,
      });
      let userId = await userPreferences.getPreferences(userPreferences.userId);

      var formData = {
        userId: userId,
        shopId: userShopId,
      };
      this.setState({
        isLoading: refreshType == 0 ? true : false,
        refreshing: refreshType == 1 ? true : false,
      });
      let serverCallHome = await HomeService.getHomeDetail(formData);

      this.setState({isLoading: false, refreshing: false});
      if (serverCallHome.status == 0) {
        this.setState({
          billOutStanding: null,
          overallSpending: null,
          paymentMade: null,
          pettyCash: null,
          otherExpense: null,
        });
        var msg = serverCallHome.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      } else if (serverCallHome.status == 2) {
        var msg = serverCallHome.msg;
        utility.showToastShort(msg);
        userPreferences.clearPreferences();
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
      } else {
        this.setState({
          billOutStanding: serverCallHome.bills_outstanding,
          overallSpending: serverCallHome.overall_spending,
          paymentMade: serverCallHome.payment_mode,
          pettyCash: serverCallHome.petty_cash,
          otherExpense: serverCallHome.other_expense,
        });
      }
    } catch (error) {
      this.setState({isLoading: false, refreshing: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  renderFilterExport = () => {
    return (
      <View>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.filterModalVisible}
          style={styles.modal}
          onRequestClose={() => {}}>
          <View style={styles.modal}>
            <ExportFilter
              completionHandler={this.filterCompletionHandler}></ExportFilter>
          </View>
        </Modal>
      </View>
    );
  };

  renderHomeDetail = () => {
    return (
      <Grid>
        <Row style={styles.overdueContainer}>
          <Text style={styles.textLabel}>Lifetime Spending</Text>
          {/* <H1 style={{ fontWeight: '500'}}>150,000 Rs</H1> */}
          <Text>
            <H1 style={{fontFamily: 'Roboto-Bold', fontSize: 24}}>
              {this.state.overallSpending != null
                ? '₹' + this.state.overallSpending
                : '₹0'}
            </H1>
          </Text>
        </Row>
        <Row size={60} style={styles.cardContainer}>
          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItem}>
              <Body style={styles.cardTextBody}>
                <Text style={styles.textLabel}>Bills Outstanding</Text>
                <Text style={[styles.textHeaderOuter, styles.textDanger]}>
                  <H1 style={[styles.textHeader, styles.textDanger]}>
                    {this.state.billOutStanding != null
                      ? '₹' + this.state.billOutStanding
                      : '₹0'}
                  </H1>
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItem}>
              <Body style={styles.cardTextBody}>
                <Text style={styles.textLabel}>Supplier Payments</Text>
                <Text style={styles.textHeaderOuter}>
                  <H1 style={styles.textHeader}>
                    {this.state.paymentMade != null
                      ? '₹' + this.state.paymentMade
                      : '₹0'}
                  </H1>
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItem}>
              <Body style={styles.cardTextBody}>
                <Text style={styles.textLabel}>Petty Cash {SCREEN_MIN_WIDTH <= 150 ? "\n ":""}</Text>
                <Text style={styles.textHeaderOuter}>
                  <H1 style={styles.textHeader}>
                    {this.state.pettyCash != null
                      ? '₹' + this.state.pettyCash
                      : '₹0'}
                  </H1>
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItem}>
              <Body style={styles.cardTextBody}>
                <Text style={styles.textLabel} >General Expenses</Text>
                <Text style={styles.textHeaderOuter}>
                  <H1 style={styles.textHeader}>
                    {this.state.otherExpense != null
                      ? '₹' + this.state.otherExpense
                      : '₹0'}
                  </H1>
                </Text>
              </Body>
            </CardItem>
          </Card>
        </Row>
        <Row size={40} style={styles.redirectContainer}>
          <Button
            onPress={() => this.addBillHandler()}
            style={styles.redirectButton}
            iconLeft
            dark
            transparent>
            <Icon name="md-paper" />
            <Text> + Add Bill</Text>
          </Button>
          <Button
            style={styles.redirectButton}
            iconLeft
            dark
            transparent
            onPress={this.addPaymentHandler}>
            <Icon type="SimpleLineIcons" name="wallet" />
            <Text>+ Add Payment</Text>
          </Button>
          <Button
            style={styles.redirectButton}
            iconLeft
            dark
            transparent
            onPress={this.exportSummaryReportHandler}>
            <Icon type="SimpleLineIcons" name="share" />
            <Text>Export Summary Report</Text>
          </Button>
        </Row>
      </Grid>
    );
  };

  render() {
    return (
      <>
        <StyleProvider style={getTheme(commonColors)}>
          <Container>
            <Content padder contentContainerStyle={styles.container}>
              <ScrollView
                contentContainerStyle={{
                  flexGrow: 1,
                  justifyContent: 'space-between',
                }}
                showsHorizontalScrollIndicator={false}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={() => {
                      this.getHomeDetail(1);
                    }}
                    tintColor={AppConstant.colorConstant.rcTintColor}
                  />
                }>
                {this.state.isLoading ? <Loader /> : this.renderHomeDetail()}
              </ScrollView>
            </Content>
          </Container>
        </StyleProvider>

        {this.state.filterModalVisible ? this.renderFilterExport() : <></>}
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    fontFamily: 'Roboto',
    backgroundColor: '#F9F9F9',
  },
  cardContainer: {
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    alignItems: 'flex-start',
  },
  overdueContainer: {
    height: 100,
    padding: 5,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  redirectContainer: {
    padding: 10,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  cardStyle: {
    flexGrow: 0,
    minWidth: SCREEN_MIN_WIDTH,
    padding: 0,
    justifyContent: 'center',
  },
  cardItem: {
    padding: 0,
  },
  cardTextBody: {
    padding: 0,
    alignItems: 'center',
  },
  textLabel: {
    fontFamily: 'Roboto',
    fontSize: 14,
    paddingBottom: 8,
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  textHeaderOuter: {
    color: '#2e2e2e',
  },
  textDanger: {
    color: '#FE3852',
  },
  textHeader: {
    fontFamily: 'Roboto-Bold',
    fontSize: 24,
    // fontWeight: "500",
    color: '#2e2e2e',
  },
  redirectButton: {
    marginBottom: 10,
    textTransform: 'capitalize',
  },
});

export default HomeScreen;
