import React from 'react';
import {
  View,
  AsyncStorage,
  StyleSheet,
  ScrollView,
  Modal,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  BackHandler,
} from 'react-native';
import {
  Container,
  Thumbnail,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Grid,
  Row,
  Toast,
  ListItem,
  List,
  ActionSheet,
  StyleProvider,
} from 'native-base';
import getTheme from '../../../native-base-theme/components';
import commonColors from '../../../native-base-theme/variables/commonColor';
import {FormStyle} from '../../styles';
import {
  EATextInput,
  EATextLabel,
  EADatePicker,
  EAPicker,
  FileItem,
} from '../../components';
import {
  isValid,
  userPreferences,
  utility,
  Enums,
  createFormData,
  AppConstant,
} from '../../utility';

import ImagePicker from 'react-native-image-picker';
import ImageViewer from 'react-native-image-zoom-viewer';
import Loader from '../Shared/Loader';
import SettleBill from '../Shared/SettleBill';
import BillService from '../../services/bills';
import SupplierService from '../../services/supplier';
import PaymentService from '../../services/payments';
import CameraScreen from '../Shared/CameraScreen';

const imageOptions = {
  title: 'Select Image',
  quality: 0.5,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class AddBillScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formType: 0,
      hasCameraPermission: null,
      billInfo: null,
      arrSuppliers: [],
      billModalVisible: false,
      paymentAmount: 0,
      paymentStatus: 0,
      date: new Date(),
      billNumber: '',
      billType: 1,
      billStatus: 0,
      supplier: 0,
      amount: '',
      description: '',
      billImage: [],
      billOldPreviewImage: [],
      billOldImage: [],
      billTypeError: '',
      billStatusError: '',
      dateError: '',
      billError: '',
      amountError: '',
      supplierError: '',
      description: '',
      formDisable: false,
      imageError: '',
      showbillImage: false,
      fullScreenImage: null,
    };

    this.SettleBillCompletionHandler = this.SettleBillCompletionHandler.bind(
      this,
    );

    this.validateImage = this.validateImage.bind(this);
    this.fileHandler = this.fileHandler.bind(this);
    this.viewFileHandler = this.viewFileHandler.bind(this);
    this.pressCancelHandler = this.pressCancelHandler.bind(this);
    this.renderBillBigImage = this.renderBillBigImage.bind(this);
  }

  static navigationOptions = {
    headerShown: false,
  };

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this.getSuppliers();
    // this.getPermissionAsyncCameraRoll();
    // this.getPermissionCameraAsync();
    const {navigation} = this.props;
    const formType = navigation.getParam('formType');

    if (formType != undefined) {
      if (formType == 1) {
        const billInfo = navigation.getParam('billInfo');
        var oldBillImage = [];
        if (billInfo.bill_image != '' && billInfo.bill_image.length != 0) {
          billInfo.bill_image.forEach(value => {
            let filename = value.substring(
              value.lastIndexOf('/') + 1,
              value.length,
            );
            var dicBillImage = {
              uri: value,
              filename: filename,
            };

            oldBillImage.push(dicBillImage);
          });
        }

        this.setState({
          billInfo: billInfo,
          formType: formType,
          date: new Date(billInfo.bill_date),
          billNumber: billInfo.bill_number + '',
          billType: billInfo.bill_type,
          billStatus: billInfo.bill_status,
          supplier: billInfo.supplier_id,
          amount: billInfo.bill_amount + '',
          description: billInfo.bill_description,
          billImage: [],
          billOldImage: billInfo.bill_image == '' ? [] : billInfo.bill_image,
          billOldPreviewImage: oldBillImage,
          formDisable: billInfo.bill_status == 2 ? true : false,
        });
      } else {
        this.setState({
          formType: formType,
          billStatus: 1,
        });
      }
    }
  }

  async componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    return false;
  };

  showOption = async () => {
    ImagePicker.showImagePicker(imageOptions, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else {
        let filename = response.uri.substring(
          response.uri.lastIndexOf('/') + 1,
          response.uri.length,
        );
        var fileDic = response;
        fileDic.filename = filename;

        var fileArr = this.state.billImage;
        fileArr.push(fileDic);

        this.setState({
          billImage: fileArr,
        });
      }
    });
  };

  setBillModalVisible(visible) {
    this.setState({billModalVisible: visible});
  }

  validateImage = async type => {
    if (this.state.formType == 1) {
      var fileCount =
        this.state.billImage.length + this.state.billOldImage.length;
      if (fileCount > 2) {
        Toast.show({
          text: 'You can select max 3 images only.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
        return;
      } else {
        this.showOption();
      }
    } else {
      var fileCount = this.state.billImage.length;

      if (fileCount > 2) {
        Toast.show({
          text: 'You can select max 3 images only.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
        return;
      } else {
        this.showOption();
      }
    }
  };

  onChangeText = key => text => {
    if (key == 'supplier' && text != 0) {
      this.setState({
        [key + 'Error']: '',
      });
    } else if (key == 'billType' && text != 0) {
      this.setState({
        [key + 'Error']: '',
      });
    } else if (key == 'billStatus' && text != 0) {
      this.setState({
        [key + 'Error']: '',
      });
    }

    this.setState({
      [key]: text,
    });
  };

  onBlurText = (validatorKey, errorKey, stateKey) => () => {
    this.setState({
      [errorKey]: isValid(validatorKey, this.state[stateKey]),
    });
  };

  getSuppliers = async () => {
    try {
      let userShopId = await userPreferences.getPreferences(
        userPreferences.userShopId,
      );
      let userId = await userPreferences.getPreferences(userPreferences.userId);
      this.setState({isLoading: true});
      let supplierData = await SupplierService.getSupplierList(
        userId,
        userShopId,
      );
      this.setState({isLoading: false});
      if (supplierData.status == 0) {
        var msg = supplierData.msg;
        Toast.show({
          text: msg,
          buttonText: 'Ok',
          type: 'success',
          duration: 5000,
        });
      } else {
        if (supplierData.supplier != null) {
          var arrSupplier = [];
          for (let i = 0; i < supplierData.supplier.length; i++) {
            let dicSupplier = {
              key: supplierData.supplier[i].id,
              text: supplierData.supplier[i].supplier_name,
            };
            arrSupplier.push(dicSupplier);
          }
          this.setState({
            arrSuppliers: arrSupplier,
          });
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text: 'Something went wrong. Please try again.',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  validate = async () => {
    let status = {valid: true, message: ''};
    let dateError = isValid('required', this.state.date);
    let amountError = isValid('amount', this.state.amount);
    let billError = isValid('required', this.state.billNumber);
    let supplierError =
      this.state.supplier == 0 ? 'Please select supplier' : '';
    let billStatusError =
      this.state.billStatus == 0 ? 'Please select bill status' : '';

    let promise = new Promise((resolve, reject) => {
      this.setState(
        {
          dateError,
          amountError,
          billError,
          supplierError,
          billStatusError,
        },
        () => {
          if (this.state.dateError) {
            status.valid = false;
            status.message = dateError;
          } else if (this.state.amountError) {
            status.valid = false;
            status.message = amountError;
          } else if (this.state.billError) {
            status.valid = false;
            status.message = billError;
          } else if (this.state.supplierError) {
            status.valid = false;
            status.message = supplierError;
          } else if (this.state.billStatusError) {
            status.valid = false;
            status.message = billStatusError;
          }
          resolve(status);
        },
      );
    });

    return promise;
  };

  submitBillForm = async () => {
    try {
      let status = await this.validate();
      if (!status.valid) {
        Toast.show({
          text: `${status.message}!`,
          buttonText: 'Ok',
          position: 'bottom',
          type: 'danger',
          duration: 5000,
        });
      } else {
        this.setState({isLoading: true});
        let userId = await userPreferences.getPreferences(
          userPreferences.userId,
        );
        let userShopId = await userPreferences.getPreferences(
          userPreferences.userShopId,
        );

        var billDate = new Date(this.state.date);
        var billDateFormatted =
          billDate.getFullYear() +
          '-' +
          (billDate.getMonth() + 1) +
          '-' +
          billDate.getDate();
        var formData = {
          bill_number: this.state.billNumber,
          bill_amount: parseInt(this.state.amount),
          bill_description: this.state.description,
          bill_date: billDateFormatted,
          bill_type: this.state.billType,
          bill_status: this.state.billStatus,
          shop_id: userShopId,
          supplier_id: this.state.supplier,
          userId: userId,
        };

        if (this.state.formType == 1) {
          formData.id = this.state.billInfo.id;
          if (this.state.billOldImage.length != 0) {
            formData.bill_old_image = this.state.billOldImage.join();
          } else {
            formData.bill_old_image = null;
          }
        }

        let multipartData = formData;
        if (this.state.billImage.length != 0) {
          multipartData = await createFormData(
            'bill_image',
            this.state.billImage,
            formData,
            true,
          );
        }

        let serverCallBill =
          this.state.formType == 0
            ? await BillService.addBill(multipartData)
            : await BillService.updateBill(multipartData);
        this.setState({isLoading: false});
        if (serverCallBill.status == 0) {
          var msg = serverCallBill.msg;
          Toast.show({
            text: msg,
            buttonText: 'Ok',
            type: 'success',
            duration: 5000,
          });
        } else {
          await userPreferences.setPreferences(userPreferences.billsTab, '1');
          await userPreferences.setPreferences(
            userPreferences.supplierTab,
            '1',
          );
          await userPreferences.setPreferences(userPreferences.homeTab, '1');
          this.setState({
            date: new Date(),
            billNumber: '',
            billType: 1,
            billStatus: 0,
            supplier: 0,
            amount: '',
            description: '',
            billOldImage: [],
            billImage: [],
          });
          var msg = serverCallBill.msg;
          Toast.show({
            text: msg,
            buttonText: 'Ok',
            type: 'success',
            duration: 5000,
          });
          this.props.navigation.goBack();
        }
      }
    } catch (error) {
      this.setState({isLoading: false}, () => {
        Toast.show({
          text:
            error && error.message
              ? error.message
              : error || 'Not Valid Error!',
          buttonText: 'Ok',
          type: 'danger',
          duration: 5000,
        });
      });
    }
  };

  SettleBillCompletionHandler(paymentAmount, paymentStatus) {
    this.setState(
      {
        paymentAmount: paymentAmount,
        paymentStatus: paymentStatus,
      },
      () => {
        this.submitPayment();
      },
    );
  }

  pressCancelHandler() {
    this.setBillModalVisible(false);
  }

  submitPayment = async () => {
    if (parseInt(this.state.paymentAmount) > parseInt(this.state.amount)) {
      Toast.show({
        text: 'Payment amount not be greater than bill amount',
        buttonText: 'Ok',
        type: 'success',
        duration: 5000,
      });
      Keyboard.dismiss();
      return;
    }

    this.setState({isLoading: true});
    let userId = await userPreferences.getPreferences(userPreferences.userId);
    let userShopId = await userPreferences.getPreferences(
      userPreferences.userShopId,
    );
    var paymentDate = new Date();
    var paymentDateFormatted =
      paymentDate.getFullYear() +
      '-' +
      (paymentDate.getMonth() + 1) +
      '-' +
      paymentDate.getDate();

    var formData = {
      category_id: 0,
      transaction_amount: parseInt(this.state.paymentAmount),
      transaction_description: ' ',
      transaction_date: paymentDateFormatted,
      bill_id: this.state.billInfo.id,
      bill_status: this.state.paymentStatus,
      transaction_mode_id: 1,
      supplier_id: this.state.billInfo.supplier_id,
      userId: userId,
      shop_id: userShopId,
      transaction_type: 3,
      transaction_pending_amount: 0,
    };

    let serverCallPayment = await PaymentService.addPayment(formData);
    this.setState({isLoading: false});
    if (serverCallPayment.status == 0) {
      var msg = serverCallPayment.msg;
      Toast.show({
        text: msg,
        buttonText: 'Ok',
        type: 'success',
        duration: 5000,
      });
    } else {
      await userPreferences.setPreferences(userPreferences.billsTab, '1');
      // await userPreferences.setPreferences(userPreferences.passbookTab, "1");
      await userPreferences.setPreferences(userPreferences.supplierTab, '1');
      await userPreferences.setPreferences(userPreferences.homeTab, '1');
      this.setBillModalVisible(false);
      var msg = serverCallPayment.msg;
      Toast.show({
        text: msg,
        buttonText: 'Ok',
        type: 'success',
        duration: 5000,
      });
      this.props.navigation.goBack();
    }
  };

  renderSettleBill = () => {
    return (
      <View>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.billModalVisible}
          style={styles.modal}
          onRequestClose={() => {}}>
          <View style={styles.modal}>
            <SettleBill
              completionHandler={this.SettleBillCompletionHandler}
              pressCancelHandler={this.pressCancelHandler}
              billInfo={this.state.billInfo}
              loading={this.state.isLoading}></SettleBill>
          </View>
        </Modal>
      </View>
    );
  };

  viewFileHandler(imageUri) {
    this.setState({
      fullScreenImage: imageUri,
      showbillImage: true,
    });
  }

  fileHandler(index, type) {
    if (type == 0) {
      var billImage = this.state.billImage;
      billImage.splice(index, 1);
      this.setState({
        billImage: billImage,
      });
    } else if (type == 1) {
      var billOldPreviewImage = this.state.billOldPreviewImage;
      billOldPreviewImage.splice(index, 1);
      var billOldImage = this.state.billOldImage;
      billOldImage.splice(index, 1);
      this.setState({
        billOldPreviewImage: billOldPreviewImage,
        billOldImage: billOldImage,
      });
    }
  }

  renderSubmit = () => {
    if (this.state.formType == 0) {
      return (
        <FooterTab>
          <Button
            full
            onPress={this.submitBillForm}
            disabled={this.state.formDisable}>
            <Text>Add Bill</Text>
          </Button>
        </FooterTab>
      );
    } else {
      return (
        <FooterTab style={styles.footer}>
          <Button
            onPress={this.submitBillForm}
            style={
              this.state.formDisable
                ? styles.buttonUpdateDisabled
                : styles.buttonUpdate
            }
            disabled={this.state.formDisable}>
            <Text>Update</Text>
          </Button>
          <Button
            onPress={() => {
              this.setBillModalVisible(true);
            }}
            style={
              this.state.formDisable
                ? styles.buttonSettleDisabled
                : styles.buttonSettle
            }
            disabled={this.state.formDisable}>
            <Text>Settle Bill</Text>
          </Button>
        </FooterTab>
      );
    }
  };

  renderBillBigImage() {
    var arrImages = [];
    var imageIndex = 0;
    var that = this;
    this.state.billImage.forEach(function(value, i) {
      if (
        that.state.fullScreenImage != null &&
        that.state.fullScreenImage === value.uri
      ) {
        
        imageIndex = i;
      }

      arrImages.push({
        url: value.uri,
        freeHeight: true,
      });
    });

    this.state.billOldImage.forEach(function(value, i) {
      if (
        that.state.fullScreenImage != null &&
        that.state.fullScreenImage === value
      ) {
        imageIndex = i + that.state.billImage.length;
      }
      arrImages.push({
        url: value,
        freeHeight: true,
      });
    });


    return (
      <Modal
        visible={this.state.showbillImage}
        transparent={true}
        onRequestClose={() =>
          this.setState({showbillImage: false, fullScreenImage: null})
        }>
        <ImageViewer
          imageUrls={arrImages}
          index={imageIndex}
          onSwipeDown={() => {
            this.setState({showbillImage: false, fullScreenImage: null});
          }}
          onMove={data => console.log(data)}
          enableSwipeDown={true}
        />
      </Modal>
    );
  }

  renderItem = (item, index) => (
    <ListItem key={index + ''}>
      <Body>
        <Text>{item.filename}</Text>
      </Body>
      <Right>
        <Icon name="arrow-forward" />
      </Right>
    </ListItem>
  );

  renderAddBill = () => {
    return (
      <>
        <Content
          padder
          contentContainerStyle={{
            flexGrow: 1,
          }}
          enabled>
          <Grid>
            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Date*'} />
              <EADatePicker
                defaultDate={
                  this.state.formType == 1
                    ? new Date(this.state.date)
                    : new Date()
                }
                minimumDate={new Date(2016, 1, 1)}
                maximumDate={new Date()}
                locale={'en'}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={'fade'}
                androidMode={'default'}
                onDateChange={this.onChangeText('date')}
                disabled={this.state.formDisable}
                formatChosenDate={date => {
                  return utility.formatDate(date);
                }}
              />
            </Row>
            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Bill Amount*'} />
              <EATextInput
                autoCapitalize="none"
                value={this.state.amount}
                keyboardType="number-pad"
                error={this.state.amountError}
                onBlur={this.onBlurText('amount', 'amountError', 'amount')}
                onChangeText={this.onChangeText('amount')}
                editable={!this.state.formDisable}
              />
            </Row>
            {this.state.formType == 1 ? (
              <Row style={FormStyle.InputSection}>
                <EATextLabel
                  labelText={
                    'Outstanding Bill Amount : ₹' +
                    (this.state.billInfo.bill_amount -
                      this.state.billInfo.partial_amount) +
                    ''
                  }
                />
              </Row>
            ) : (
              <></>
            )}

            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Bill Number*'} />
              <EATextInput
                autoCapitalize="characters"
                value={this.state.billNumber}
                error={this.state.billError}
                onBlur={this.onBlurText('required', 'billError', 'billNumber')}
                onChangeText={this.onChangeText('billNumber')}
                editable={!this.state.formDisable}
              />
            </Row>
            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Supplier*'} />
              <EAPicker
                note
                mode="dropdown"
                selectedValue={this.state.supplier}
                option={this.state.arrSuppliers}
                // iosIcon={<Icon name="arrow-down" />}
                error={this.state.supplierError}
                onValueChange={this.onChangeText('supplier')}
                enabled={!this.state.formDisable}
              />
            </Row>
            {/* {this.state.formType == 1 ? (
                <Row style={FormStyle.InputSection}>
                  <EATextLabel labelText={"Bill Status"} />
                  <EAPicker
                    note
                    mode="dropdown"
                    selectedValue={this.state.billStatus}
                    option={Enums.billStatus}
                    error={this.state.billStatusError}
                    //   iosIcon={<Icon name="arrow-down" />}
                    onValueChange={this.onChangeText("billStatus")}
                  />
                </Row>
              ) : (
                <></>
              )} */}
            <Row style={FormStyle.InputSection}>
              <EATextLabel labelText={'Description'} />
              <EATextInput
                autoCapitalize="sentences"
                value={this.state.description}
                keyboardType="default"
                //     error={this.state.descriptionError}
                onChangeText={this.onChangeText('description')}
                editable={!this.state.formDisable}
              />
            </Row>
            <Row style={FormStyle.fileInputSection}>
              <Button dark transparent activeOpacity={1}>
                <Text style={FormStyle.inputLabel}>Attachments</Text>
              </Button>
              <Button
                onPress={this.validateImage}
                style={FormStyle.attachButton}
                dark
                transparent
                disabled={this.state.formDisable}>
                <Icon name="ios-add" style={FormStyle.attachIcon} />
              </Button>
            </Row>
          </Grid>
          <View style={FormStyle.fileSection}>
            {this.state.billImage.length > 0 ? (
              <View>
                {this.state.billImage.map((value, index) => {
                  return (
                    <FileItem
                      pressHandler={this.fileHandler}
                      viewFileHandler={this.viewFileHandler}
                      fileData={value}
                      index={index}
                      key={index + 'new'}
                      type={0}
                      disabled={this.state.formDisable}></FileItem>
                  );
                })}
              </View>
            ) : (
              <></>
            )}
            {this.state.billOldPreviewImage.length > 0 ? (
              <View>
                {this.state.billOldPreviewImage.map((value, index) => {
                  return (
                    <FileItem
                      pressHandler={this.fileHandler}
                      viewFileHandler={this.viewFileHandler}
                      fileData={value}
                      index={index}
                      key={index + 'old'}
                      type={1}
                      disabled={this.state.formDisable}></FileItem>
                  );
                })}
              </View>
            ) : (
              <></>
            )}
          </View>
        </Content>
        <Footer>{this.renderSubmit()}</Footer>
      </>
    );
  };

  render() {
    return (
      <>
        <StyleProvider style={getTheme(commonColors)}>
          <Container>
            <Header noShadow >
              <Left>
                <Button
                  transparent
                  onPress={() => this.props.navigation.goBack()}>
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              <Body>
                <Title style={FormStyle.headerColor}>
                  {this.state.formType == 0 ? 'Add Bill' : 'Edit Bill'}
                </Title>
              </Body>
              <Right />
            </Header>
            {this.state.isLoading ? <Loader /> : this.renderAddBill()}
            {/* {this.renderAddBill()} */}
          </Container>
        </StyleProvider>
        {this.state.billModalVisible ? this.renderSettleBill() : <></>}
        {this.renderBillBigImage()}
      </>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 0,
    backgroundColor: '#FFFFFF',
  },
  buttonUpdate: {
    flex: 1,
    marginRight: 2,
    backgroundColor: '#FE3852',
  },
  buttonSettle: {
    flex: 1,
    marginLeft: 2,
    backgroundColor: '#6ACB67',
  },
  buttonUpdateDisabled: {
    flex: 1,
    marginRight: 2,
    backgroundColor: '#A7A7A7',
  },
  buttonSettleDisabled: {
    flex: 1,
    marginLeft: 2,
    backgroundColor: '#A7A7A7',
  },
});

export default AddBillScreen;
