import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Col,
  Grid,
  Row,
  StyleProvider,
} from 'native-base';

import getTheme from '../../../native-base-theme/components';
import commonColors from '../../../native-base-theme/variables/commonColor';
import {ToolbarHeader, FormStyle} from '../../styles';
import {utility} from '../../utility';
class AboutScreen extends Component {
  static navigationOptions = {
    headerShown: false,
  };
  constructor(props) {
    super(props);
    this.state = {
      contact: '8055607475',
      email: 'earlystack2020@gmail.com',
    };
    this.emailTapped = this.emailTapped.bind(this);
    this.contactTapped = this.contactTapped.bind(this);
  }

  emailTapped = async () => {
    utility.openEmail(this.state.email)
  };

  contactTapped = async () => {
    utility.phoneCall(this.state.contact);
  };

  render() {
    return (
      <StyleProvider style={getTheme(commonColors)}>
        <Container>
          <Header noShadow>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title style={FormStyle.headerColor}>Support</Title>
              
            </Body>
            <Right></Right>
          </Header>
          <Content padder contentContainerStyle={styles.container}>
            <Grid style={styles.item}>
              {/* <Row style={styles.row}>
                <Col size={50}>
                  <Row>
                    <Text style={styles.subTitle}>Support</Text>
                  </Row>
                </Col>
                <Col size={50}></Col>
              </Row> */}
              <Row style={styles.row10}>
                <Text style={styles.description}>
                  EarlyStack is a technology company and we are building simple
                  and real solutions for India's 60 million-strong micro, small
                  and medium businesses.
                </Text>
              </Row>
              <Row style={styles.row10}>
                <Text style={styles.description}>
                  Our first offering – an App that will enable businesses to
                  digitally record and manage various aspects of business
                  operations like suppliers, payments, expenses, accounts
                  payable and more. We believe this will assist businesses to
                  manage their operations better, save time and eventually help
                  in reducing their overall cost of operations
                </Text>
              </Row>
              <Row style={styles.row10}>
                <Text style={styles.description}>
                  New features will be added soon to help you discover and
                  connect with more suppliers, get better prices and offers,
                  inventory analysis, GST filings and managing all payments from
                  a single platform.
                </Text>
              </Row>
              <Row style={styles.row30}>
                <Text style={styles.description}>
                  EarlyStack intends to be your technology partner and help you
                  succeed in today's digital world
                </Text>
              </Row>
              <Row style={styles.row}>
                <Col size={100} style={styles.dangerCol}>
                  <Button style={styles.listButton} iconLeft dark transparent onPress={this.contactTapped}>
                    <Icon
                      type="MaterialIcons"
                      name="phone"
                      style={styles.iconText}
                    />
                    <Text style={styles.dangerTitle}>+91 8055607475</Text>
                  </Button>
                </Col>
                <Col size={0}></Col>
              </Row>
              <Row style={styles.row}>
                <Col size={100} style={styles.dangerCol}>
                  <Button
                    style={styles.listButton}
                    iconLeft
                    dark
                    transparent
                    onPress={this.emailTapped}>
                    <Icon
                      type="MaterialIcons"
                      name="mail"
                      style={styles.iconText}
                    />
                    <Text style={styles.dangerTitle}>
                      earlystack2020@gmail.com
                    </Text>
                  </Button>
                </Col>
                <Col size={0}></Col>
              </Row>
              <Row style={styles.rowVersion}>
              <Text note style={styles.subTitleVersion}>Version 1.0</Text>
              </Row>
            </Grid>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    flexGrow: 1,
    justifyContent: 'space-between',
    padding: 5,
    marginTop: 10,
  },
  row: {
    marginBottom: 10,
  },
  row10: {
    marginBottom: 10,
  },
  row30: {
    marginBottom: 30,
  },
  dangerCol: {
    alignItems: 'flex-start',
  },
  description: {
    fontSize: 15,
    color: '#2e2e2e',
  },
  subHeaderIcon: {
    fontSize: 20,
  },
  subTitle: {
    color: '#637381',
    fontSize: 18,
    fontWeight: '500',
  },
  rowVersion:{
    flexDirection:'row',
    justifyContent: 'center',
    alignItems:'center',
    padding: 5,
    marginTop: 5,
  },
  subTitleVersion:{
    color: '#909090',
    fontSize: 15,
    fontFamily:'Roboto-Black'
  },
  dangerTitle: {
    fontSize: 18,
    color: '#FE3852',
  },
  listButton: {
    margin: 0,
  },
  iconText: {
    height: 40,
    width: 40,
    fontSize: 40,
    marginLeft: 0,
    marginRight: 0,
  },
});

export default AboutScreen;
