import { Root } from 'native-base';
import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  View,
} from 'react-native';

import { Ionicons, Feather } from 'react-native-vector-icons';
import { isValid, utility, userPreferences } from "../utility";
class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    let userId = await userPreferences.getPreferences(userPreferences.userId);
    if (userId != null) {
      let userShopId = await userPreferences.getPreferences(
        userPreferences.userShopId
      );
      if (userShopId != null) {
        this.props.navigation.navigate("App");
      } else {
        this.props.navigation.navigate("Shops");
      }
    }else{
      this.props.navigation.navigate("Auth");
    }
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    // this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
        <StatusBar barStyle="default" backgroundColor="#FE3852" />
      </View>
    );
  };
}

export default AuthLoadingScreen;